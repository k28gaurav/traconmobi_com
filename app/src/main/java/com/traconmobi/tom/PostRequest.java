package com.traconmobi.tom;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Timeout;


/**
 * Created by kumargaurav on 3/29/16.
 */
public class PostRequest {
    private String response1 = "";
    private String url;
    RequestBody parameter;
    OkHttpClient client;
    String TAG = "PostRequest";

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public AsyncResponse delegate = null;

    public PostRequest(AsyncResponse delegate, RequestBody params, String url) {
        this.delegate = delegate;
        this.parameter = params;
        this.url = url;
    }

    public String doInBackground() {
        try {
            try {
                client = new OkHttpClient();
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.getMessage());

            }

            Request request = new Request.Builder()
                    .url(url)
                    .post(parameter)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "UserRequest failure in PostRequest" + call.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response != null) {

                        if (response.isSuccessful()) {
                            try {
                                response1 = response.body().string();
                            } catch (NullPointerException e) {
                            } catch
                                    (OutOfMemoryError e) {
                                Log.e(TAG, "Out of memory" + e.getMessage());
                            }
                            if (response1 != null || response1 != "") {
                                Log.e(TAG, "sending sync request....\n");
                                delegate.processFinish(response1);
                            }
                        }
                    }
                }
            });
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return response1;
    }
}

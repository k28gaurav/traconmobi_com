
/**
 * Created by Ashwini Naik on 11/10/15.
 */
package com.traconmobi.tom;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.crashlytics.android.Crashlytics;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import io.fabric.sdk.android.Fabric;


public class RVPOrderDetails extends FragmentActivity {

    ViewPager pager;
    RVPAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"Details","Map"};
    //    int Numboftabs =3;
    int Numboftabs =1;
    public static String employeeId="";
    public static String pickId="";
    SessionManager session;
    public static final String PREF_NAME1 = "Pager";
    public static FragmentManager fragmentManager;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    TextView title;
    String FORM_TYPE_VAL;
    public static final String PREF_NAME = "Pager";
    public String TAG="RVPOrderDetails";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_rvpickup_orderdetails);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_assgn_dtls_main);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("ORDER DETAILS");

            fragmentManager = getSupportFragmentManager();
            Intent intnt = getIntent();
            FORM_TYPE_VAL = intnt.getStringExtra("NAV_FORM_TYPE_VAL");
            employeeId = intnt.getStringExtra("waybillNo");
            pickId = intnt.getStringExtra("pickUpId");
            Log.e(TAG, "EXTRA_MESSAGE" + employeeId);
            TextView tv_ordno = (TextView) findViewById(R.id.txtordnoval);
            tv_ordno.setText(employeeId);
            session = new SessionManager(getApplicationContext());

            // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
            adapter = new RVPAdapter(getSupportFragmentManager(), Titles, Numboftabs, employeeId, pickId, FORM_TYPE_VAL);

            // Assigning ViewPager View and setting the adapter
            pager = (ViewPager) findViewById(R.id.pager);
            pager.setAdapter(adapter);

            // Assiging the Sliding Tab Layout View
            tabs = (SlidingTabLayout) findViewById(R.id.tabs);
            tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

            // Setting Custom Color for the Scroll bar indicator of the Tab View
            tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
                @Override
                public int getIndicatorColor(int position) {
                    return getResources().getColor(R.color.tabsScrollColor);
                }
            });

            // Setting the ViewPager For the SlidingTabsLayout
            tabs.setViewPager(pager);
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
        }
    }

    public void toggleMenu(View v)
    {
        try
        {
            finish();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "toggleMenu" + e.getMessage());
        }

    }

    @Override
    public void onBackPressed()
    {

    }
    public void onclk_noti(View v)
    {
        Intent homeActivity = new Intent(this, NotificationActivity.class);
        startActivity(homeActivity);
    }

    public void onclk_trip(View v)
    {
        Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
        startActivity(homeActivity);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.assgn_dtls_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

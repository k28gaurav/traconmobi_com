package com.traconmobi.tom.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("AssignmentNo")
    private String AssignmentNo;

    @SerializedName("CompanyId")
    private String CompanyId;

    @SerializedName("Latitude")
    private String Latitude;

    @SerializedName("LocID")
    private String LocID;

    @SerializedName("Longitude")
    private String Longitude;

    @SerializedName("UID")
    private String UID;

    @SerializedName("completeStatus")
    private String completeStatus;

    @SerializedName("pickupReason")
    private String pickupReason;

    @SerializedName("reasonId")
    private String reasonId;

    @SerializedName("pickupRemark")
    private String pickupRemark;

    @SerializedName("scanItem")
    private List<String> scanItem = new ArrayList<String>();

    /**
     *
     * @return
     * The AssignmentNo
     */
    public String getAssignmentNo() {
        return AssignmentNo;
    }

    /**
     *
     * @param AssignmentNo
     * The AssignmentNo
     */
    public void setAssignmentNo(String AssignmentNo) {
        this.AssignmentNo = AssignmentNo;
    }

    /**
     *
     * @return
     * The CompanyId
     */
    public String getCompanyId() {
        return CompanyId;
    }

    /**
     *
     * @param CompanyId
     * The CompanyId
     */
    public void setCompanyId(String CompanyId) {
        this.CompanyId = CompanyId;
    }

    /**
     *
     * @return
     * The LocID
     */
    public String getLocID() {
        return LocID;
    }

    /**
     *
     * @param LocID
     * The LocID
     */
    public void setLocID(String LocID) {
        this.LocID = LocID;
    }

    /**
     *
     * @return
     * The UID
     */
    public String getUID() {
        return UID;
    }

    /**
     *
     * @param UID
     * The UID
     */
    public void setUID(String UID) {
        this.UID = UID;
    }

    /**
     *
     * @return
     * The completeStatus
     */
    public String getCompleteStatus() {
        return completeStatus;
    }

    /**
     *
     * @param completeStatus
     * The completeStatus
     */
    public void setCompleteStatus(String completeStatus) {
        this.completeStatus = completeStatus;
    }

    /**
     *
     * @return
     * The pickupReason
     */
    public String getPickupReason() {
        return pickupReason;
    }

    /**
     *
     * @param pickupReason
     * The pickupReason
     */
    public void setPickupReason(String pickupReason) {
        this.pickupReason = pickupReason;
    }

    /**
     *
     * @return
     * The reasonId
     */
    public String getReasonId() {
        return reasonId;
    }

    /**
     *
     * @param reasonId
     * The reasonId
     */
    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    /**
     *
     * @return
     * The pickupRemark
     */
    public String getPickupRemark() {
        return pickupRemark;
    }

    /**
     *
     * @param pickupRemark
     * The pickupRemark
     */
    public void setPickupRemark(String pickupRemark) {
        this.pickupRemark = pickupRemark;
    }

    /**
     *
     * @return
     * The scanItem
     */
    public List<String> getScanItem() {
        return scanItem;
    }

    /**
     *
     * @param scanItem
     * The scanItem
     */
    public void setScanItem(List<String> scanItem) {
        this.scanItem = scanItem;
    }

    public String getLatitude() {
        return Latitude;
    }

    /**
     *
     * @param Latitude
     * The Latitude
     */
    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    /**
     *
     * @param Longitude
     * The Longitude
     */
    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

}
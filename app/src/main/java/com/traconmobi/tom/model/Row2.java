package com.traconmobi.tom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kumargaurav on 2/9/16.
 */
public class Row2 {

    @SerializedName("id")
    private String id;

    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private Value2 value;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The key
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     * The key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     * The value
     */
    public Value2 getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(Value2 value) {
        this.value = value;
    }
}

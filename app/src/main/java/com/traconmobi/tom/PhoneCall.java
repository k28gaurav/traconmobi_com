package com.traconmobi.tom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class PhoneCall {
	
	public static final String PARAM_CALL_DONE = "CALL_DONE";
	 
    public static void call(String phoneNumber, Activity activity)
    {
    	try
    	{
        CallEndedListener.createListenerFor(activity);
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        activity.startActivity(callIntent);
	    }
		catch(Exception e)
		{
			System.out.println("Exception PhoneCall " + e.getMessage());
		}
    }
 
    private static class CallEndedListener extends PhoneStateListener 
    {
        private boolean called = false;
        private final TelephonyManager telephonyManager;
        private Activity activity;
 
        private CallEndedListener(Activity act)
        {

           this.activity = act;
            this.telephonyManager = (TelephonyManager) act
                    .getSystemService(Context.TELEPHONY_SERVICE);
        }
 
        public static void createListenerFor(Activity act)
        {

        	try
        	{
            CallEndedListener listener = new CallEndedListener(act);
            listener.telephonyManager.listen(listener, LISTEN_CALL_STATE);
	        }
			catch(Exception e)
			{
				System.out.println("Exception createListenerFor " + e.getMessage());
			}
        }
        @SuppressWarnings({ "unused", "null" })
		protected void onResume()
        {
        	try
        	{
        	Bundle extras=null;
            if (extras.getBoolean(PhoneCall.PARAM_CALL_DONE)) {
               // showInfoText(); // do whatever you like here...
         
                extras.remove(PhoneCall.PARAM_CALL_DONE);
            }
	        }
			catch(Exception e)
			{
				System.out.println("Exception onCallStateChanged onResume " + e.getMessage());
			}
        }
        @Override
        public void onCallStateChanged(int state, String incomingNumber) 
        {
        	try
        	{
            if (called && state == TelephonyManager.CALL_STATE_IDLE) {
                called = false;
                telephonyManager.listen(this, PhoneStateListener.LISTEN_NONE);
                try {
                    Intent t = new Intent(activity, activity.getClass());
                    t.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    t.putExtras(activity.getIntent());
                    t.putExtra(PARAM_CALL_DONE, true);
                    t.setAction(Intent.ACTION_MAIN);
                    activity.finish();
                    activity = null;
                    activity.startActivity(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    called = true;
                }
            }
	        }
			catch(Exception e)
			{
				System.out.println("Exception onCallStateChanged " + e.getMessage());
			}
        }
    }

 
    
}


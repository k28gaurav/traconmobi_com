package com.traconmobi.tom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.kobjects.base64.Base64;

import com.crashlytics.android.Crashlytics;


//import net.sqlcipher.database.SQLiteConstraintException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;

import android.app.ProgressDialog;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import io.fabric.sdk.android.Fabric;

public class AssignedMultipleUndelActivity extends Activity implements OnClickListener, OnSaveClickListener {

    // The minimum distance to change Updates in meters
//    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = (float) 11.1 ; // 10 meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    OnSaveClickListener listener;
//    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * randBetween(0, 5) ;

    Location location; // location
    String undelReason = "";

    public String selectedItemTag, rsn_desc, rsn_id, reason_code, reason_id, consgnee, get_assgn_id, get_lat, get_lng,
            SESSION_TRANSPORT, new_lat, new_lon, session_COD_AMT, session_AWB_COUNT, session_RESPONSE_AWB_NUM, result, entityString_gps, imei_num;
    TextView awb_total_cnt, cod_total;
    SQLiteDatabase db, db_bkr;
    String Remarks, showdate, showtime, st;
    private Button cancelButton, saveButton;
    int mMonth, mYear, mDay, hour, minute;
    static final String EXTRA_MESSAGE = null;
    public static String employeeId, aa, photoname = null;
    public static int shw_cnt;
    String PageName, PageName1, count_del_sync, count_del;
    Bundle bundle;
    TextView version_name, progresstextview;
    Cursor cursor, c, c_routesyn, c_chk_custmr, cr;
    String currentTime, photo_dt, photo_tme;

    double latitude, longitude, net_lat, net_lon;
    protected static final String MULTIPLE_UNDEL_ASSGN_ID = null;
    protected static final String FORM_TYPE = null;

    private boolean clicked = false; // this is a member variable
    static File image;
    File fileimage = null;
    final int TAKE_PHOTO_CODE = 1;
    protected Button imageview;

    TableLayout tl;
    int clickcount = 0;
    int j = 1;


    byte[] BAvalue;
    public static String getphotobytearray, s_photo;
    static ImageView iv;
    static Bitmap theImage;
    private String selectedImagePath;

    static AutoCompleteTextView et_reason;
    TextView title;
    StringBuffer responseText_photonme;
    int count = 0;
    List<String> cust_labels;
    int var;
    EditText remarks;
    // Session Manager Class
    SessionManager session;
    TinyDB tiny;
    public String session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT,
            session_CUST_ACC_CODE, session_USER_NAME, session_DB_PATH, session_DB_PWD;
    public static String TAG = "AssignedMultipleUndelActivity";
    int count_custmr;

    public static int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_undelivery_layout);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_undelivery_layout);

            title = (TextView) findViewById(R.id.txt_title);
            saveButton = (Button) findViewById(R.id.btnsave);
            cancelButton = (Button) findViewById(R.id.btncancel);
            remarks = (EditText) findViewById(R.id.txtremarks);
            et_reason = (AutoCompleteTextView) findViewById(R.id.edt_reason);
            session = new SessionManager(getApplicationContext());
            tiny = new TinyDB(getApplicationContext());
            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

/********************$$$$$$$$$$$$$$$$$$$$*************/
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleUndelActivity onCreate " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity UnsatisfiedLinkError ");
        }
    }

    public void onResume() {
        super.onResume();

        try {

            listener = this;
            // get AssgnMultiple_num_Dts data from session
            HashMap<String, String> AssgnMultiple_num_Dts = session.getAssgnMultiple_num_Details();

            // DB_PATH
            session_RESPONSE_AWB_NUM = AssgnMultiple_num_Dts.get(SessionManager.KEY_RESPONSE_AWB_NUM);

            // get AuthenticateDb data from session
            HashMap<String, String> AssgnMultiple_Dts = session.getAssgnMultipleDetails();

            // DB_PATH
            session_AWB_COUNT = AssgnMultiple_Dts.get(SessionManager.KEY_AWB_COUNT);

            // DB_PWD
            session_COD_AMT = AssgnMultiple_Dts.get(SessionManager.KEY_COD_AMT);

            responseText_photonme = new StringBuffer();

            String shw[] = session_RESPONSE_AWB_NUM.split(",");
            for (int i = 0; i < shw.length; i++) {
                result = shw[i];
                result = result.replace("/", "");
                result = result.replace(" ", "");
                result = result.replace("-", "");
                result = result.replace(":", "");
            }
            title.setText("UNDELIVERY UPDATE");

            SESSION_TRANSPORT = tiny.getString("transport_val");

            try {
                LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                LocationListener mlocListener = new MyLocationListener();
                if (location == null) {
                    mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, mlocListener);


                    if (mlocManager != null) {
                        location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            new_lat = String.valueOf(roundToDecimals(location.getLatitude(), 4));
                            new_lon = String.valueOf(roundToDecimals(location.getLongitude(), 4));
                        }
                    }
                }

            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMCODUpdateActivity onCreate LocationManager " + e.getMessage() + e.getStackTrace().toString());
            }

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            Date currentLocalTime = cal.getTime();
            DateFormat date1 = new SimpleDateFormat("HH:mm:ss z");
            date1.setTimeZone(TimeZone.getTimeZone("GMT"));
            String localTime = date1.format(currentLocalTime);
            Crashlytics.log(android.util.Log.ERROR, TAG, "undellocaltime  " + localTime);

            cancelButton.setOnClickListener(this);
            saveButton.setOnClickListener(this);

/********************$$$$$$$$$$$$$$$$$$$$*************/
            cust_labels = new ArrayList<String>();
            // show the list view as dropdown
            Crashlytics.log(android.util.Log.ERROR, TAG, "print val" + et_reason.getText().toString().trim().toUpperCase());
            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
//			    cust_labels.add("SELECT");

            String flg = "N";
            //
            Cursor cr = db.rawQuery("select T_UD_Reason_Desc,_id from UD_REASON_MASTER where T_UD_Reason_Desc LIKE '%" + et_reason.getText().toString().trim() + "%' and T_Cust_Acc_NO='" + session_CUST_ACC_CODE + "' group by T_UD_Reason_Desc", null);
            while (cr.moveToNext()) {
                reason_code = cr.getString(cr.getColumnIndex("T_UD_Reason_Desc"));
                cust_labels.add(reason_code);

            }
            int count = cr.getCount();
            cr.close();
            //Crashlytics.log(android.util.Log.ERROR, TAG, "print count" + count);
            db.close();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, cust_labels);

            et_reason.setAdapter(adapter);
            et_reason.setThreshold(1);
            et_reason.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
//					reasonText[0] = " ";
                    if (cust_labels.contains(et_reason.getText().toString().trim())) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "in list");
                    } else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Not in list");
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        } catch (SQLException e) {

        } catch (Exception e) {

        }

    }

    public void OnBtnClear(View v) {
        et_reason.setText("");

    }

    @Override
    public void saveData() {
        new CursorTask().execute();
    }

    public class MyLocationListener implements LocationListener {

        @Override

        public void onLocationChanged(Location loc) {

            new_lat = String.valueOf(roundToDecimals(loc.getLatitude(), 4));

            new_lon = String.valueOf(roundToDecimals(loc.getLongitude(), 4));

            if (new_lat == null) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
                new_lat = "null";
            } else {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
            }
            if (new_lon == null) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
                new_lon = "null";
            } else {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
            }


        }

        @Override

        public void onProviderDisabled(String provider)

        {

        }

        @Override

        public void onProviderEnabled(String provider) {
        }

        @Override

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }/* End of Class MyLocationListener */

    private File getFile(Context context) {
        try {
            final File path = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/com.traconmobi.parekh/", "Image keeper");
            photoname = null;
            if (!path.exists()) {
                path.mkdir();
            }
            try {
//	    	   File sdcard = Environment.getExternalStorageDirectory();
                File sdcard = new File(Environment.getExternalStorageDirectory()
                        + "/Android/data/com.traconmobi.parekh");
//	    	   File mymirFolder = new File(sdcard.getAbsolutePath()+ "/Image keeper'" + session_CURRENT_DT + "' /");
//	    	    File mymirFolder = new File(sdcard.getAbsolutePath() + "/Image keeper/");
                File mymirFolder = new File(sdcard.getAbsolutePath() + "/Image keeper/");
                String shw[] = session_RESPONSE_AWB_NUM.split(",");

                Crashlytics.log(android.util.Log.ERROR, TAG, "print photo count " + count);
                for (int i = 0; i < shw.length; i++) {
                    result = shw[i];

                    result = result.replace("/", "");
                    result = result.replace(" ", "");
                    result = result.replace("-", "");
                    result = result.replace(":", "");


                    photoname = result + "_" + count + ".png";
                    Crashlytics.log(android.util.Log.ERROR, TAG, "print multi photoname" + photoname);
//			       photoname.replace("/", "");
                    fileimage = new File(path, photoname);


                }
//		        Crashlytics.log(android.util.Log.ERROR,TAG,"print responseText_photonme getFile "+ responseText_photonme);
//		        count=count+1;
                if (!mymirFolder.exists()) {
                    File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
                    noMedia.mkdirs();
                    noMedia.createNewFile();
                }
            } catch (IOException e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception getFile  SQLiteException" + e.getMessage());
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleDelActivity getFile " + e.getMessage());
            //onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleDelActivity UnsatisfiedLinkError ");
            //onClickGoToHomePage();
            //        	    			finish();
        }
        return fileimage;


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
//       	super.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                //********************************Activity Result for Photo/Image **************************//
                case TAKE_PHOTO_CODE:

                    if (requestCode == TAKE_PHOTO_CODE) {
                        if (resultCode == RESULT_OK) {

                            try {
                                final Calendar c = Calendar.getInstance();
                                String month = c.get(Calendar.MONTH) + 1 + "";
                                if (month.length() < 2) {
                                    month = "0" + month;
                                }
                                String date = c.get(Calendar.DAY_OF_MONTH) + "";
                                if (date.length() < 2) {
                                    date = "0" + date;
                                }
                                mYear = c.get(Calendar.YEAR);
                                mMonth = c.get(Calendar.MONTH);
                                mDay = c.get(Calendar.DAY_OF_MONTH);
                                hour = c.get(Calendar.HOUR_OF_DAY);
                                minute = c.get(Calendar.MINUTE);
                                photo_dt = "" + c.get(Calendar.YEAR) + "-" + month + "-" +
                                        date;
                                showtime = " " +
                                        c.get(Calendar.HOUR) + ":" +
                                        c.get(Calendar.MINUTE) + ":" +
                                        c.get(Calendar.SECOND);

                                photo_tme = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                                        (c.get(Calendar.MINUTE)) + ":" +
                                        (c.get(Calendar.SECOND));
                                Log.w("TIME:", String.valueOf(photo_tme) + currentTime);
//           	                  photo_dt=showdate;
//           	                  photo_tme=String.valueOf(currentTime);
                                image = getFile(AssignedMultipleUndelActivity.this);
                                clicked = true;
                                if (session_DB_PATH != null && session_DB_PWD != null) {
//   		        	            	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                    String shw[] = session_RESPONSE_AWB_NUM.split(",");
                                    for (int i = 0; i < shw.length; i++) {
                                        result = shw[i];

                                        result = result.replace("/", "");
                                        result = result.replace(" ", "");
                                        result = result.replace("-", "");
                                        result = result.replace(":", "");
                                        String strFilter = "T_Assignment_Number='" + result + "' ";
//   		        	            	String strFilter = "T_Assignment_Number=" + result;.

                                        ContentValues cv = new ContentValues();
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "print TAKE_PHOTO_CODE count " + count + photoname);
//   		        	              	photoname= result +"_"+count + ".png";
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "print db values " + photoname);
                                        cv.put("T_Photo", photoname);
                                        db.update("TOM_Assignments", cv, strFilter, null);
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "print cv values " + cv);
                                    }
                                    db.close();
                                } else {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMCODUpdateActivity step 4 TOMLoginUserActivity.file is null ");
                                    //onClickGoToHomePage();
                                }
                                //displaying image
                                if (photoname != null && photoname != "" && !(photoname.equals("null"))) {
                                    image = new File(Environment.getExternalStorageDirectory()
                                            + "/Android/data/com.traconmobi.parekh/", "Image keeper/" + photoname.trim());
//	    				    	+ "/Image keeper'" + session_CURRENT_DT + "' /"
                                    selectedImagePath = Environment.getExternalStorageDirectory()
                                            + "/Android/data/com.traconmobi.parekh" + "/Image keeper/" + photoname.trim();
                                    Bitmap bm = reduceImageSize(selectedImagePath);

                                    if (bm != null) {

                                        ByteArrayOutputStream BAO = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 40, BAO);

                                        BAvalue = BAO.toByteArray();
                                        getphotobytearray = Base64.encode(BAvalue);
                                        s_photo = getphotobytearray.replace("+", "%2B");
//		    				    Crashlytics.log(android.util.Log.ERROR,TAG,"s_photo " + s_photo);
                                    } else if (bm == null) {
                                        getphotobytearray = "null";
                                    }
                                } else if (photoname == null && photoname == "" && photoname.equals("null")) {
                                    getphotobytearray = "null";
                                }

                                Crashlytics.log(android.util.Log.ERROR, TAG, "print getphotobytearray " + getphotobytearray);
                                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                                byte[] ba2 = Base64.decode(getphotobytearray);

                                ByteArrayInputStream imageStream_sign = new ByteArrayInputStream(ba2);
                                theImage = BitmapFactory.decodeStream(imageStream_sign);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);
                                clickcount++;
                                count = count + 1;
                                responseText_photonme.append(photoname + "/");
//		                        }


                                Toast toast = Toast
                                        .makeText(this, "Photo capture Successfull", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                            } catch (OutOfMemoryError o) {
                                o.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Assng OutOfMemoryError " + o.getMessage());

                            } catch (SQLiteException e) {
                                // TODO: handle exception
                                e.printStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Assng " + e.getMessage());

                            } catch (UnsatisfiedLinkError err) {
                                err.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleDelActivity UnsatisfiedLinkError ");

                            } catch (Exception o) {
                                o.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception " + o.getMessage());

                            } finally {
                                if (db != null) {
                                    db.close();
                                }
                                // This block contains statements that are ALWAYS executed
                                // after leaving the try clause, regardless of whether we leave it:
                                // 1) normally after reaching the bottom of the block;
                                // 2) because of a break, continue, or return statement;
                                // 3) with an exception handled by a catch clause above; or
                                // 4) with an uncaught exception that has not been handled.
                                // If the try clause calls System.exit(), however, the interpreter
                                // exits before the finally clause can be run.
                            }
                            break;

                        } else if (resultCode == RESULT_CANCELED) {
                            photoname = "null";
//                 Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                            Toast toast = Toast
                                    .makeText(this, "Picture was not taken", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();

                        } else {
                            photoname = "null";
//                 Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                            Toast toast = Toast
                                    .makeText(this, "Picture was not taken", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();

                        }
                    } else {
                        photoname = "null";
                        Toast toast = Toast
                                .makeText(this, "No Photo captured", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.BOTTOM, 105, 50);
                        toast.show();
                    }
                    //********************************Activity Result for signature **************************//
            }
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleDelActivity onActivityResult " + e.getMessage());

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleDelActivity onActivityResult UnsatisfiedLinkError ");
        }
    }


    public Bitmap reduceImageSize(String selectedImagePath) {
        Bitmap m = null;
        try {
            File f = new File(selectedImagePath);

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 150;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Image File not found in your phone. Please select another image.", Toast.LENGTH_LONG).show();
        }
        return m;
    }

    public void toggleMenu(View v) {
        finish();
    }

    @Override
    public void onClick(View v) {
        try {
            if (v.getId() == R.id.btncancel) {
                Intent goToNextActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                startActivity(goToNextActivity);
//			finish();
            } else if (v.getId() == R.id.btnsave) {
                final Calendar c = Calendar.getInstance();
                String month = c.get(Calendar.MONTH) + 1 + "";
                if (month.length() < 2) {
                    month = "0" + month;
                }
                String date = c.get(Calendar.DAY_OF_MONTH) + "";
                if (date.length() < 2) {
                    date = "0" + date;
                }
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                showdate = "" + c.get(Calendar.YEAR) + "-" + month + "-" +
                        date;
                showtime = " " +
                        c.get(Calendar.HOUR) + ":" +
                        c.get(Calendar.MINUTE) + ":" +
                        c.get(Calendar.SECOND);

                currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                        (c.get(Calendar.MINUTE)) + ":" +
                        (c.get(Calendar.SECOND));
                Log.w("TIME:", String.valueOf(currentTime) + currentTime);


                Remarks = remarks.getText().toString().trim();


                if (et_reason.getText().toString().isEmpty() || et_reason.getText().toString() == null) {

                    Toast toast = Toast
                            .makeText(this, "Please Select the Reason !!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    saveButton.setEnabled(true);

                    return;
                } else if (!et_reason.getText().toString().isEmpty()) {
                    if (!remarks.getText().toString().trim().isEmpty()) {
                        Remarks = remarks.getText().toString().trim();
                    } else {
                        Remarks = "null";
                    }
                    if (cust_labels.contains(et_reason.getText().toString().trim())) {
                        undelReason = et_reason.getText().toString().trim();
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                        // Setting Dialog Title
                        alertDialog.setTitle("Alert");

                        // Setting Dialog Message
                        alertDialog.setMessage("Are you sure you want to confirm ?");

                        // On pressing Settings button
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    if (listener != null) {
                                        Log.e(TAG, "Save the data");
                                        listener.saveData();
                                    }
                                    dialog.dismiss();
                                } catch (SQLException s) {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Exception db path SQLException " + s.getMessage());
                                } catch (Exception e) {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Exception db path  " + e.getMessage());
                                } finally {

                                }

                            }
                        });

                        // on pressing cancel button
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//		            	Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//		    			 startActivity(goToNextActivity);
                                saveButton.setEnabled(true);
//							 saveButton.setBackgroundResource(R.drawable.button);
                                dialog.cancel();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.create().show();

                    } else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Not in list");
                        Toast.makeText(getApplicationContext(), "Please select reason from the list", Toast.LENGTH_LONG).show();
                    }
                }

            }
        } catch (SQLiteException e) {
            //onClickGoToHomePage();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity onclick SQLiteException ");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity onclick Exception ");
            //onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity onclick UnsatisfiedLinkError ");
            //onClickGoToHomePage();
//			finish();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //**********************modification for menu options(Home & Logout)**************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                onClickGoToHomePage();
                return true;
            case R.id.menu_log_out:
                onClickLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onClickLogOut() {
        try {
            Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
            logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutActivity);
        } catch (SQLiteConstraintException ex) {
            //what ever you want to do
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity onClickLogOut ");

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity UnsatisfiedLinkError ");

        }
    }

    private void onClickGoToHomePage() {
        try {
            Intent homePageActivity = new Intent(this, HomeMainActivity.class);
//        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homePageActivity);
        } catch (SQLiteConstraintException ex) {
            //what ever you want to do
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity onClickGoToHomePage ");

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity UnsatisfiedLinkError ");

        }
    }

    private class CursorTask extends AsyncTask<Void, Void, Void> {
        final ProgressDialog dialog = new ProgressDialog(AssignedMultipleUndelActivity.this);


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for Report Building...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }


        CursorTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                if (session_DB_PATH != null && session_DB_PWD != null) {
                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                    Cursor cr = db.rawQuery("select T_UD_Reason_Desc,T_UD_Reason_Id,_id from UD_REASON_MASTER where T_UD_Reason_Desc='" +
                            undelReason + "'", null);
                    while (cr.moveToNext()) {
                        reason_code = cr.getString(cr.getColumnIndex("T_UD_Reason_Desc"));
                        reason_id = cr.getString(cr.getColumnIndex("T_UD_Reason_Id"));
                    }
                    if (SESSION_TRANSPORT.equals("BIKER")) {
                        if (session_DB_PATH != null && session_DB_PWD != null) {
                            String shw[] = session_RESPONSE_AWB_NUM.split(",");
                            for (int i = 0; i < shw.length; i++) {
                                result = shw[i];
                                cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Assignment_Id,emp.T_Assignment_Type," +
                                                "emp.T_Consignee_Name," +
                                                "emp.T_Address_Line1,emp.T_Address_Line2,emp.T_City,emp.T_Contact_number," +
                                                "emp.T_Pincode,emp.T_OUT_Scan_U_ID,emp.T_Out_Scan_Location," +
                                                "mgr.T_IMEI FROM TOM_Assignments emp JOIN TOM_Users  mgr ON mgr.T_U_ID= emp.T_OUT_Scan_U_ID  " +
                                                "where emp.T_Assignment_Number = ?",
                                        new String[]{"" + result});

                                while (cursor.moveToNext()) {
                                    String awbnumber = cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
                                    imei_num = cursor.getString(cursor.getColumnIndex("T_IMEI"));
                                    String awbid = cursor.getString(cursor.getColumnIndex("T_Assignment_Id"));
                                    String assgn_type = cursor.getString(cursor.getColumnIndex("T_Assignment_Type"));
                                    String u_id = cursor.getString(cursor.getColumnIndex("T_OUT_Scan_U_ID"));
                                    String out_loc_id = cursor.getString(cursor.getColumnIndex("T_Out_Scan_Location"));

                                    ContentValues cv = new ContentValues();
                                    cv.put("D_In_Complete_Date", showdate);
                                    cv.put("D_In_Complete_Time", String.valueOf(currentTime));
                                    cv.put("T_Assignment_In_Complete_Reason", undelReason);
                                    cv.put("T_Assignment_In_Complete_Reason_Id", reason_id);
                                    cv.put("T_Remarks", Remarks);
                                    cv.put("T_U_ID", u_id);
                                    cv.put("T_Out_Scan_Location", out_loc_id);
                                    cv.put("T_Assignment_Number", awbnumber);
                                    cv.put("T_Assignment_Id", awbid);
                                    cv.put("T_Assignment_Type", assgn_type);
                                    cv.put("T_AccquiredLat", new_lat);
                                    cv.put("T_AccquiredLon", new_lon);

                                    cv.put("D_Last_Updated_date_time", showdate + "-" + String.valueOf(currentTime));
                                    if (clicked == false) {
                                        photoname = "null";
                                    } else {
                                        photoname = responseText_photonme.toString();
                                    }
                                    cv.put("T_Photo", photoname);
                                    cv.put("T_Photo_dt", photo_dt);
                                    cv.put("T_Photo_tm", String.valueOf(photo_tme));
                                    cv.put("T_Cust_Acc_NO", session_CUST_ACC_CODE);
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "print responseText_photonme step 2" + responseText_photonme);
                                    try {
                                        db.insertOrThrow("Tom_Assignments_In_Complete", null, cv);
                                        String set_flg = "FALSE";
                                        Cursor c = db.rawQuery("UPDATE TOM_Assignments SET B_is_Completed='" + set_flg + "' " +
                                                "WHERE T_Assignment_Number='" + result + "'", null);
                                        while (c.moveToNext()) {
                                        }
                                        c.close();
                                    } catch (SQLiteConstraintException ex) {
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity insertOrThrow ");
                                    }

                                }
                                cursor.close();
                            }
                            db.close();
                        }
                    } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                        if (session_DB_PATH != null && session_DB_PWD != null) {
                            String shw[] = session_RESPONSE_AWB_NUM.split(",");
                            for (int i = 0; i < shw.length; i++) {
                                result = shw[i];
                                cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Assignment_Id,emp.T_Assignment_Type," +
                                                "emp.T_Consignee_Name,emp.T_Address_Line1,emp.T_Address_Line2,emp.T_City,emp.T_Contact_number," +
                                                "emp.T_Pincode,emp.T_OUT_Scan_U_ID,emp.T_Out_Scan_Location,mgr.T_IMEI FROM TOM_Assignments emp " +
                                                "JOIN TOM_Users  mgr ON mgr.T_U_ID= emp.T_OUT_Scan_U_ID  where emp.T_Assignment_Number = ?",
                                        new String[]{"" + result});

                                while (cursor.moveToNext()) {
                                    String awbnumber = cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
                                    imei_num = cursor.getString(cursor.getColumnIndex("T_IMEI"));
                                    String awbid = cursor.getString(cursor.getColumnIndex("T_Assignment_Id"));
                                    String assgn_type = cursor.getString(cursor.getColumnIndex("T_Assignment_Type"));
                                    String u_id = cursor.getString(cursor.getColumnIndex("T_OUT_Scan_U_ID"));
                                    String out_loc_id = cursor.getString(cursor.getColumnIndex("T_Out_Scan_Location"));

                                    ContentValues cv = new ContentValues();
                                    cv.put("D_In_Complete_Date", showdate);
                                    cv.put("D_In_Complete_Time", String.valueOf(currentTime));
                                    cv.put("T_Assignment_In_Complete_Reason", undelReason);
                                    cv.put("T_Assignment_In_Complete_Reason_Id", reason_id);
                                    cv.put("T_Remarks", Remarks);
                                    cv.put("T_U_ID", u_id);
                                    cv.put("T_Out_Scan_Location", out_loc_id);
                                    cv.put("T_Assignment_Number", awbnumber);
                                    cv.put("T_Assignment_Id", awbid);
                                    cv.put("T_Assignment_Type", assgn_type);
                                    cv.put("T_AccquiredLat", new_lat);
                                    cv.put("T_AccquiredLon", new_lon);

                                    Crashlytics.log(android.util.Log.ERROR, TAG, "undelivered latitude " + latitude + "longitude " + longitude);
                                    cv.put("D_Last_Updated_date_time", showdate + "-" + String.valueOf(currentTime));
                                    if (clicked == false) {
                                        photoname = "null";
                                    } else {
                                        photoname = responseText_photonme.toString();
                                    }
                                    cv.put("T_Photo", photoname);
                                    cv.put("T_Photo_dt", photo_dt);
                                    cv.put("T_Photo_tm", String.valueOf(photo_tme));
                                    cv.put("T_Cust_Acc_NO", session_CUST_ACC_CODE);
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "print responseText_photonme step 2" + responseText_photonme);
                                    try {
                                        db.insertOrThrow("Tom_Assignments_In_Complete", null, cv);
                                        String strFilter = "T_Assignment_Number='" + result + "' ";
                                        String set_flg = "FALSE";
                                        Cursor c = db.rawQuery("UPDATE TOM_Assignments SET B_is_Completed='" + set_flg + "' " +
                                                "WHERE T_Assignment_Number='" + result + "'", null);
//					 					   				new String[]{""+result});
                                        while (c.moveToNext()) {
                                        }
                                        c.close();
                                    } catch (SQLiteConstraintException ex) {
                                        //what ever you want to do
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleUndelActivity insertOrThrow ");
                                    }

                                }
                                cursor.close();
                            }
                            db.close();
                        }
                    }
                } else {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMUndeliveredActivity TOMLoginUserActivity.file is null ");
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (SQLException e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                db.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void reult) {

            saveButton.setEnabled(false);
            Crashlytics.log(android.util.Log.ERROR, TAG, "cust get_assgn_id" + get_assgn_id + get_lat + get_lng);
            Intent goToNextActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
            Toast.makeText(AssignedMultipleUndelActivity.this, "Record Saved succesfully", Toast.LENGTH_LONG).show();
            startActivity(goToNextActivity);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

        }
    }

}

package com.traconmobi.tom;

/**
 * Created by LENOVO on 11/16/2015.
 */
//public class ParseRelation_XmlResponse {
//}

//package com.navatech.internlmodule;
//
//public class ParseHoldReason_XmlResponse {
//
//}

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;

//import net.sqlcipher.database.SQLiteConstraintException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import android.provider.BaseColumns;
import android.util.Xml;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteConstraintException;

public class ParseRelation_XmlResponse {

    private ArrayList<HashMap<String,String>> parsedRelationList;
    protected SQLiteDatabase db;
    private StringReader xmlReader;

    public String relation_id_Value=null;
    public String relation_type_Value=null;
//    public String cust_locate_acc_no_Value=null;
//    public String cust_locate_id_Value=null;

    TinyDB tinydb;
//	public String DEL_isPickedValue=null;

    public static String relation_idVal,relation_typVal;

    private final String starttagList = "RELATION";
    private final String relation_id = "RELATION_ID";
    private final String relation_type = "RELATION_TYPE";

    // Session Manager Class
    SessionManager session;
    public String session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;
    // variable to hold context
    public Context context;
    protected SQLiteDatabase db_insert;
    public String TAG="ParseRelation_XmlResponse";
    Cursor cursor;

    public ParseRelation_XmlResponse(String xml,Context context)
    {
        try
        {
            xmlReader = new StringReader(xml);
            this.context=context;

            //Intializing Fabric
            Fabric.with(context, new Crashlytics());
            // Session class instance
            session = new SessionManager(context);
            tinydb = new TinyDB(context);

            /**GETTING SESSION VALUES**/

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);

            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);

        }
        catch(Exception e)
        {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ParseLoginAuthenticateXmlResponse" + e.getMessage());
        }

//		xmlReader = new StringReader(xml);

    }

    public void parse() throws XmlPullParserException, IOException
    {


        TreeMap<String, String> RelationListObj = null;
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(xmlReader);
        int eventType = parser.getEventType();

        RelationListObj = new TreeMap<String, String>();

        parsedRelationList = new ArrayList<HashMap<String, String>>();
        while (eventType != XmlPullParser.END_DOCUMENT)
        {
            String xmlNodeName = parser.getName();
            if (XmlPullParser.START_TAG == eventType)
            {
                xmlNodeName = parser.getName();
                if (xmlNodeName.equalsIgnoreCase(relation_id))
                {
                    relation_id_Value = parser.nextText().toString();
//				Log.d("AWB_ID",outscan_awbidValue);			
                    RelationListObj.put("relation_id",relation_id_Value);
//					Crashlytics.log(android.util.Log.ERROR,TAG,"trnsfr_usr_id_Value " + trnsfr_usr_id_Value);
                }
//
                else if (xmlNodeName.equalsIgnoreCase(relation_type))
                {
                    relation_type_Value = parser.nextText().toString();
                    RelationListObj.put("relation_type", relation_type_Value);
                }
            } else if (XmlPullParser.END_TAG == eventType)
            {
                if (xmlNodeName.equalsIgnoreCase(starttagList))
                {
            parsedRelationList.add(new HashMap<String, String>(

                            RelationListObj));
                    for (Map.Entry<String, String> entry : RelationListObj.entrySet())
                    {
                        relation_idVal =RelationListObj.get("relation_id");
                        relation_typVal =RelationListObj.get("relation_type");
                        ContentValues cv =new ContentValues();
//				    cv.put("T_U_ID",u_id);			
                        cv.put("T_RELATION_ID", relation_id_Value);
//                        tinydb.putString("Locate_Assgn_id",cust_assgn_id_Value);
                        cv.put("T_RELATION_TYP",relation_type_Value);

                        try{
                            if(session_DB_PATH != null && session_DB_PWD != null)
                            {
                                try {

                                    db_insert = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);

                                    db_insert.insertWithOnConflict("TBL_Relation_mstr", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_REPLACE);
                                }catch(SQLException e) {

                                }catch(Exception e) {

                                }
                                }
                            else
                            {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Error Login parser session_DB_PATH is null ");
                            }
                        }
                        catch(SQLiteConstraintException ex){

                            //what ever you want to do		
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception session_DB_PATH " + ex.getMessage());
                        }
                        finally
                        {
                            if(db_insert != null && db_insert.isOpen())
                            {
                                db_insert.close();
                            }
                        }
                    }
                    RelationListObj.clear();
                }
            }
            eventType = parser.next();
        }
    }

    @SuppressWarnings("unused")
    private String readText(XmlPullParser parser) throws IOException,

            XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    public ArrayList getparsedDelCompleteList() {
        return parsedRelationList;
    }
}


package com.traconmobi.tom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.couchbase.lite.LiveQuery;
import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

public class PickUpAssignedList extends Fragment {

    /**
     * Items entered by the user is stored in this ArrayList variable
     */
    ArrayList<String> list = new ArrayList<String>();

    CommunicationChannel mCommChListner;
    OnItemTouchListener itemTouchListener;
    //ArrayList<String> uniquelist = new ArrayList<String>();

    /**
     * Declaring an ArrayAdapter to set items to ListView
     */
    ArrayAdapter<String> adapter;
    HashMap<String, String> docsId;

    private RecyclerView mRecyclerView;
    private CardViewDataAdapter mAdapter;
    SessionManager session;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText searchtext;
    private CouchBaseDBHelper cDB;
    public String SESSION_TRANSPORT;
    CardViewDataAdapter getfiltr;
    ImageView searchIcon, removeText;
    TinyDB tiny;
    String[] completeList;
    public String TAG = "PickUpAssignedList";

    final ArrayList<String> assign_no = new ArrayList<>();
    final ArrayList<String> assign_id = new ArrayList<>();
    final ArrayList<String> companyId = new ArrayList<>();

    final ArrayList<PickUpCustomData> dataAdapter = new ArrayList<PickUpCustomData>();
    final ArrayList<PickUpCustomData> saveDataAdapter = new ArrayList<PickUpCustomData>();
    final ArrayList<PickUpCustomData> addressAdapter = new ArrayList<PickUpCustomData>();
    /*******************
     * Commented on 13nov2015   setting default page
     ***********************/

    ArrayList<String> incompleteList = new ArrayList<String>();
    ArrayList<String> addressinfo = new ArrayList<>();

    Date curDate = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    final String mLastUpdateTime = dateFormat.format(curDate).toString();

    Set<String> setPickUp;
    Object[] dataId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pickup_assgnlist, container, false);
        try {

            //Intializing Fabric
            Fabric.with(getActivity(), new Crashlytics());
            session = new SessionManager(getActivity().getApplicationContext());
            tiny = new TinyDB(getActivity().getApplicationContext());
            SESSION_TRANSPORT = tiny.getString("transport_val");

            // sendMessage(1);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv);
            searchtext = (EditText) rootView.findViewById(R.id.searchScan);
            searchIcon = (ImageView) rootView.findViewById(R.id.searchItem);
            removeText = (ImageView) rootView.findViewById(R.id.remove);

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            cDB = new CouchBaseDBHelper(getActivity().getApplicationContext());

            // Set<String> setPickUp = pref.getStringSet("pickuplist", null);

            setPickUp = session.getPickUpId(session.getUserDetails().get(session.KEY_USER_NAME));
            // navigation.changeNav(1);

            if (setPickUp != null) {
                itemTouchListener = new OnItemTouchListener() {
                    @Override
                    public void onCardViewTap(View view, int position) {
                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                            // Setting Dialog Title
                            alertDialog.setTitle("Alert");

                            // Setting Dialog Message
                            alertDialog.setMessage("Please start the trip");
                            alertDialog.setCancelable(false);
                            // On pressing Settings button
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_PICKUP_SCAN");
                                    startActivity(goToNextActivity);
                                }
                            });
                            alertDialog.show();
                        } else {
                            try {
                                if (session.getAssignType(dataAdapter.get(position).getmAssignNo()).equalsIgnoreCase("RVP")) {
                                    Intent i = new Intent(getActivity().getApplicationContext(), RVPOrderDetails.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                    i.putExtra("waybillNo", session.getWayBillNo(dataAdapter.get(position).getmAssignNo()));
                                    i.putExtra("pickUpId", dataAdapter.get(position).getmAssignNo());
                                    startActivity(i);
                                } else if (session.getAssignType(dataAdapter.get(position).getmAssignNo()).equalsIgnoreCase("O")) {
                                    Intent i = new Intent(getActivity().getApplicationContext(), ECOMUserActivity.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                    i.putExtra("pickId", dataAdapter.get(position).getmAssignNo());
                                    i.putExtra("assignId", dataAdapter.get(position).getmAssignId());
                                    i.putExtra("companyId", dataAdapter.get(position).getmCompanyId());
                                    //i.putStringArrayListExtra("listId", list);
                                    startActivity(i);
                                } else {
                                    //Toast.makeText(getActivity().getApplicationContext(), "Tapped " +dataAdapter.get(position), Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity().getApplicationContext(), PickUpScanList.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                    i.putExtra("pickId", dataAdapter.get(position).getmAssignNo());
                                    i.putExtra("assignId", dataAdapter.get(position).getmAssignId());
                                    i.putExtra("companyId", dataAdapter.get(position).getmCompanyId());
                                    //i.putStringArrayListExtra("listId", list);
                                    startActivity(i);
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Oncardview click" + e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onButtonCancelClick(View view, int position) {
                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                            // Setting Dialog Title
                            alertDialog.setTitle("Alert");

                            // Setting Dialog Message
                            alertDialog.setMessage("Please start the trip");
                            alertDialog.setCancelable(false);
                            // On pressing Settings button
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_PICKUP_CANCEL");
                                    startActivity(goToNextActivity);
                                }
                            });


                            // Showing Alert Message
                            alertDialog.show();
                        } else {
                            try {
                                if (session.getAssignType(dataAdapter.get(position).getmAssignNo()).equalsIgnoreCase("O")) {
                                    Intent i = new Intent(getActivity().getApplicationContext(), ECOMCancelUpdate.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                    i.putExtra("assignmentId", dataAdapter.get(position).getmAssignId());
                                    i.putExtra("assignmentNo", dataAdapter.get(position).getmAssignNo());
                                    i.putExtra("companyId", dataAdapter.get(position).getmCompanyId());
                                    //i.putStringArrayListExtra("listId", list);
                                    startActivity(i);
                                } else if (session.getAssignType(dataAdapter.get(position).getmAssignNo()).equalsIgnoreCase("P")) {
                                    Intent i = new Intent(getActivity().getApplicationContext(), PickUpCancelUpdate.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("assignmentId", dataAdapter.get(position).getmAssignId());
                                    i.putExtra("assignmentNo", dataAdapter.get(position).getmAssignNo());
                                    i.putExtra("companyId", dataAdapter.get(position).getmCompanyId());
                                    getActivity().getApplicationContext().startActivity(i);
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception cancel click" + e.getMessage());
                            }
                        }
                        //Toast.makeText(getActivity().getApplicationContext(), "Clicked Button1 in " + dataAdapter.get(position), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onButtonMapClick(View view, int position) {
                        try {
                            String shipAdd1 = addressAdapter.get(position).getmShipAdd1();
                            String shipAdd2 = addressAdapter.get(position).getmShipAdd2();
                            String shipCity = addressAdapter.get(position).getmShipCity();
                            String shipPin = addressAdapter.get(position).getmShipPin();
                            Crashlytics.log(android.util.Log.ERROR, TAG, "print shipAdd1" + shipAdd1 + shipAdd2 + shipCity + shipPin);
                            if (shipAdd1 == null && shipAdd2 == null && shipCity == null && shipPin == null) {
                                Toast.makeText(getActivity().getApplicationContext(), "Address not available" + addressAdapter.get(position), Toast.LENGTH_SHORT).show();
                            } else if (shipAdd1 != null && shipAdd2 != null && shipCity != null && shipPin != null) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/maps?daddr=" + shipAdd1 + shipAdd2 + shipCity + shipPin + "&dirflg=r"));
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception map click" + e.getMessage());
                        }
                    }

                    @Override
                    public void onButtonCallClick(View view, int position) {
                        try {
                            String shipContactnum = addressAdapter.get(position).getmShipContact();

                            Crashlytics.log(android.util.Log.ERROR, TAG, "print shipContactnum" + shipContactnum);
                            if (shipContactnum == null) {
                                Toast.makeText(getActivity().getApplicationContext(), "Contact number not available" + addressAdapter.get(position), Toast.LENGTH_SHORT).show();
                            } else if (shipContactnum != null) {
                                PhoneCall phcl = new PhoneCall();
                                phcl.call(shipContactnum, getActivity());
                            }
                        } catch (Exception e) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception map click" + e.getMessage());
                        }

                    }

                    @Override
                    public void onRVPTextClick(View view, int position) {
                        Intent i = new Intent(getActivity().getApplicationContext(), RVPOrderDetails.class);
                        i.putExtra("waybillNo", session.getWayBillNo(dataAdapter.get(position).getmAssignNo()));
                        i.putExtra("pickUpId", dataAdapter.get(position).getmAssignNo());
                        startActivity(i);
                    }
                };
            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Pickup Oncreate" + e.getMessage());
        }
        return rootView;
    }

    interface CommunicationChannel {
        public void setCommunication(int i);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "on Start called");
        SESSION_TRANSPORT = tiny.getString("transport_val");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CommunicationChannel) {
            mCommChListner = (CommunicationChannel) activity;
        } else {
            throw new ClassCastException();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "on Resume called");

        try {
            setPickUp = session.getPickUpId(session.getUserDetails().get(session.KEY_USER_NAME));
            if (setPickUp != null) {
                dataId = setPickUp.toArray(new String[setPickUp.size()]);
                cDB = new CouchBaseDBHelper(getActivity().getApplicationContext());

                for (Object ob : dataId) {
                    if (ob instanceof String) {
                        String[] sData = ((String) ob).split(":::");
                        //System.out.print(sData);
                        assign_no.add(sData[0]);
                        assign_id.add(sData[1]);
                        companyId.add(sData[7]);
                        incompleteList.add(sData[0] + "_" + sData[1] + "_" + sData[8] + "_" + mLastUpdateTime);
                        addressinfo.add(sData[0] + "_" + sData[8] + "_" + mLastUpdateTime);
                    }
                }

                for (Object ob : dataId) {
                    if (ob instanceof String) {
                        String[] sData = ((String) ob).split(":::");
                        //System.out.print(sData);

                        int i = 0;
                        for (String status : incompleteList) {
                            boolean statusFlag = cDB.retrieveAllPickUpStatus(status);
                            if (statusFlag && status.contains(sData[0])) {
                                dataAdapter.add(new PickUpCustomData(sData[0], sData[2], "Shipper Account No: " + sData[5], "Pickup Time: " + sData[6], "No of Pcs: " + sData[9], sData[1].toString(), sData[8].toString()));
                            }
                        }

                        for (String address : addressinfo) {
                            Map<String, Object> getAddress = cDB.getShipperAdress(address);
                            if (address.contains(sData[0])) {
                                addressAdapter.add(new PickUpCustomData((String) getAddress.get("shipAdd1"), (String) getAddress.get("shipAdd2"), (String) getAddress.get("shipPin"), (String) getAddress.get("shipCity"), (String) getAddress.get("shipConct")));
                            }
                        }
                    }
                }
                saveDataAdapter.addAll(dataAdapter);
                mAdapter = new CardViewDataAdapter(getActivity().getApplicationContext(), dataAdapter, itemTouchListener);
                mRecyclerView.setAdapter(mAdapter);

                searchtext.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                        removeText.setVisibility(View.GONE);
                        searchIcon.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {


                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                    }
                });

                searchIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchIcon.setVisibility(View.GONE);
                        removeText.setVisibility(View.VISIBLE);
                        ArrayList<PickUpCustomData> currentData = new ArrayList<PickUpCustomData>();
                        String searchTest = searchtext.getText().toString();
                        for (PickUpCustomData pData : dataAdapter) {
                            if (pData.getmAssignNo().contains(searchTest)) {
                                currentData.add(pData);
                            }
                        }
                        dataAdapter.clear();
                        dataAdapter.addAll(currentData);
                        mAdapter.notifyDataSetChanged();
                    }
                });

                removeText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchIcon.setVisibility(View.VISIBLE);
                        removeText.setVisibility(View.GONE);
                        searchtext.setText("");
                        dataAdapter.clear();
                        dataAdapter.addAll(saveDataAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        }catch(Exception e) {

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause called");
        incompleteList.clear();
        addressinfo.clear();
        dataAdapter.clear();
        addressAdapter.clear();
        saveDataAdapter.clear();
    }

    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop called");
    }


}




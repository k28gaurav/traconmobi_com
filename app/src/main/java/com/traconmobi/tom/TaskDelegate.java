package com.traconmobi.tom;

/**
 * Created by kumargaurav on 3/30/16.
 */
public interface TaskDelegate {
    void TaskCompletionResult(boolean result);
}

package com.traconmobi.tom;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import java.util.ArrayList;


public class AssignedListAdapter extends FragmentPagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    String ord_no;
    ArrayList<Fragment> fragments = new ArrayList<Fragment>();
    private BaseFragment  mFragmentAtPos0;
    private FragmentManager mFragmentManager;
    public String TAG="AssignedListAdapter";

    private static final String STR_CHILD_TAG_2 = " -> child fragment of tag 2";


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public AssignedListAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, String ordrno) {
        super(fm);
        mFragmentManager = fm;
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.ord_no = ordrno;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position)
    {
        if(position == 0) {
            try
            {

            if (mFragmentAtPos0 == null) {
                Log.e(TAG, "At switch from Single to Multiple fragment");
                mFragmentAtPos0 = DeliverySingleFragment.newInstance(new PageFragmentListener() {
                    public void onSwitchToNextFragment() {
                        mFragmentManager.beginTransaction().remove((Fragment) mFragmentAtPos0).commit();
                        mFragmentAtPos0 = DeliveryMultipleFragment.newInstance();
                        mFragmentAtPos0.setShowingChild(true);
                        notifyDataSetChanged();
                    }
                });

        }

        }
            catch(Exception e)
            {
                Crashlytics.log(android.util.Log.ERROR, TAG, "getItem" + e.getMessage());
            }
            return (Fragment) mFragmentAtPos0;
        }
        else //if(position == 1)            // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            PickUpAssignedList pagesFragment = new PickUpAssignedList();
            return pagesFragment;
        }

    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        //return fragments.size();
        return NumbOfTabs;
    }
    @Override
    public int getItemPosition(Object object)
    {
        if (object instanceof DeliverySingleFragment && mFragmentAtPos0 instanceof DeliveryMultipleFragment) {
        return POSITION_NONE;
    }
//        return POSITION_UNCHANGED;
      else if (object instanceof DeliveryMultipleFragment && mFragmentAtPos0 instanceof DeliverySingleFragment) {
            return POSITION_NONE;
        }
        else if(object instanceof DeliveryMultipleFragment) {
            return POSITION_NONE;
        }
        return POSITION_UNCHANGED;
    }

    public void replaceChildFragment(BaseFragment oldFrg, int position) {
        switch (position) {
            case 0:
                try
                {
                mFragmentManager.beginTransaction().remove((Fragment) oldFrg).commit();
                mFragmentAtPos0 = DeliverySingleFragment.newInstance(new PageFragmentListener() {
                    public void onSwitchToNextFragment() {
                        mFragmentManager.beginTransaction().remove((Fragment) mFragmentAtPos0).commit();
                        mFragmentAtPos0 = DeliveryMultipleFragment.newInstance();
                        mFragmentAtPos0.setShowingChild(true);
                        notifyDataSetChanged();
                    }
                });
                notifyDataSetChanged();
                break;
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "replaceChildFragment" + e.getMessage());
        }

            default:
                break;
}
}

}
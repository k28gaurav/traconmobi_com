package com.traconmobi.tom.singeleton;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kumargaurav on 6/7/16.
 */

public class UserParser implements Serializable {

    private Integer Sync;
    private Integer UserId;
    private String UserName;
    private String Password;
    private Integer CompanyID;
    private String CompanyName;
    private Integer LocID;
    private String LocCode;
    private Long IMEI;
    private Object BatteryUsage;
    private String LastLoginDateTime;
    private String ServerDateTime;
    private Integer CityId;
    private Integer LocationTypeID;
    private Integer ParentHub;
    private String Token;
    private String Timezone;
    private String baseurl;
    private String pickup;
    private String rvp;
    private String ecom;

    private UserParser(){}

    public static UserParser parser;
    public static UserParser getInstance(){
        if(parser == null) {
            parser = new UserParser();
        }
        return parser;
    }

    public Integer getSync() {
        return Sync;
    }

    public void setSync(Integer sync) {
        Sync = sync;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public Integer getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(Integer companyID) {
        CompanyID = companyID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public Integer getLocID() {
        return LocID;
    }

    public void setLocID(Integer locID) {
        LocID = locID;
    }

    public String getLocCode() {
        return LocCode;
    }

    public void setLocCode(String locCode) {
        LocCode = locCode;
    }

    public Long getIMEI() {
        return IMEI;
    }

    public void setIMEI(Long IMEI) {
        this.IMEI = IMEI;
    }

    public Object getBatteryUsage() {
        return BatteryUsage;
    }

    public void setBatteryUsage(Object batteryUsage) {
        BatteryUsage = batteryUsage;
    }

    public String getLastLoginDateTime() {
        return LastLoginDateTime;
    }

    public void setLastLoginDateTime(String lastLoginDateTime) {
        LastLoginDateTime = lastLoginDateTime;
    }

    public String getServerDateTime() {
        return ServerDateTime;
    }

    public void setServerDateTime(String serverDateTime) {
        ServerDateTime = serverDateTime;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public Integer getLocationTypeID() {
        return LocationTypeID;
    }

    public void setLocationTypeID(Integer locationTypeID) {
        LocationTypeID = locationTypeID;
    }

    public Integer getParentHub() {
        return ParentHub;
    }

    public void setParentHub(Integer parentHub) {
        ParentHub = parentHub;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getTimezone() {
        return Timezone;
    }

    public void setTimezone(String timezone) {
        Timezone = timezone;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getRvp() {
        return rvp;
    }

    public void setRvp(String rvp) {
        this.rvp = rvp;
    }

    public String getEcom() {
        return ecom;
    }

    public void setEcom(String ecom) {
        this.ecom = ecom;
    }
}

package com.traconmobi.tom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ViewGroup.LayoutParams;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

public class ECOMUserActivity extends Activity {

    ArrayAdapter adapter;
    CouchBaseDBHelper dbHelper;
    SessionManager sessionManager;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    public static final String PREF_NAME = "ScanPref";
    public static final String PREF_NAME1 = "Pager";
    public int scn_cnt = 0;
    public String FORM_TYPE_VAL;
    private final String TAG = "ECOMUserActivity";
    TextView title, tv_scn_count;
    String username, pickUpId, assignId, companyId, locationId;
    ListView listView;
    Button completeButton;
    EditText scan;
    Set<String> setScanData = new HashSet<>();
    Set<String> setScanRefData = new HashSet<>();
    List<String> list = new ArrayList<String>();
    List<String> savedList = new ArrayList<String>();
    List<String> compareScanList = new ArrayList<String>();
    Set<String> barcodeScan = new HashSet<>();
    String docId;
    String mLastUpdateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_ecom_pickup);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("SCAN LIST");

            sessionManager = new SessionManager(getApplicationContext());
            username = sessionManager.getUserDetails().get(sessionManager.KEY_USER_NAME);

            Intent intnt = getIntent();
            FORM_TYPE_VAL = intnt.getStringExtra("NAV_FORM_TYPE_VAL");
            Crashlytics.log(android.util.Log.ERROR, TAG, "PRINT AND shw form type" + FORM_TYPE_VAL);

            pickUpId = getIntent().getStringExtra("pickId");
            assignId = getIntent().getStringExtra("assignId");
            companyId = getIntent().getStringExtra("companyId");

            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            mLastUpdateTime = dateFormat.format(curDate).toString();


            docId = pickUpId + "_" + assignId + "_" + companyId + "_" + mLastUpdateTime;

            locationId = sessionManager.getLocId();

            listView = (ListView) findViewById(R.id.scanList);
            tv_scn_count = (TextView) findViewById(R.id.pickup_count);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

//        if(pickUpId != null) {
        /*pref = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
            final Set<String> setScanData = pref.getStringSet(pickUpId, null);
*/
            setScanData = sessionManager.getScannedData(pickUpId);
            setScanRefData = sessionManager.getScanRef(pickUpId);
            if (setScanData != null) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "pickUpId" + pickUpId + "size" + setScanData.size());
            }

            String[] scanData;
            if (setScanRefData != null) {
                scanData = setScanRefData.toArray(new String[setScanRefData.size()]);
                compareScanList = Arrays.asList(scanData);
                Log.e(TAG, "Compare List" + compareScanList);
            }

            String[] dataId;
            if (setScanData != null) {
                dataId = setScanData.toArray(new String[setScanData.size()]);
                savedList = Arrays.asList(dataId);
                barcodeScan.addAll(savedList);
                Crashlytics.log(android.util.Log.ERROR, TAG, "PickUpLis insde" + savedList + "size" + setScanData.size() + "barcodeScan" + barcodeScan.size());
                scn_cnt = scn_cnt + barcodeScan.size();
                tv_scn_count.setText(Integer.toString(scn_cnt));
            }

            listView.setAdapter(adapter);
            dbHelper = new CouchBaseDBHelper(getApplicationContext());

            completeButton = (Button) findViewById(R.id.complete);

            TextView pick = (TextView) findViewById(R.id.pickupId);
            pick.setText(pickUpId);

            scan = (EditText) findViewById(R.id.scanId);
            scan.requestFocus();
            final Handler handler = new Handler();

            scan.setOnKeyListener(new android.view.View.OnKeyListener() {
                @Override
                public boolean onKey(android.view.View v, int keyCode, KeyEvent event) {
                    //scan.setText("");
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {

                        String scannedData = scan.getText().toString().trim();
                        Crashlytics.log(android.util.Log.ERROR, "Barcode value", scannedData);
                        scan.setFocusable(true);
                        scan.requestFocus();
                        if (scannedData.isEmpty() && scannedData.trim().length() < 0) {
                            Toast.makeText(getApplicationContext(), "Please scan AWB's", Toast.LENGTH_LONG).show();
                        } else if (!(scannedData.isEmpty()) && scannedData.trim().length() > 0) {
//				       adding contact to contact list
                            if (barcodeScan.contains(scannedData)) {

                                //Ignore duplicate values
                                Log.e(TAG, "print duplicate");
                                scan.setText("");
                            } else if (!compareScanList.contains(scannedData)) {
                                //Ignore duplicate values
                                //playBeep();
                                ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                                toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);
                                popWindow();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Scan waybill didnt match");
                                scan.setText("");
                            } else {
                                list.add(0, scannedData);
                            }
                            adapter.notifyDataSetChanged();
                            barcodeScan.addAll(list);
                            scn_cnt = barcodeScan.size();
                            tv_scn_count.setText(Integer.toString(scn_cnt));
                            scan.setText("");
                            sessionManager.setScannedData(barcodeScan, pickUpId);
                        }
                    }

                    // Put focus back in the EditText after brief delay
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scan.setFocusable(true);
                            scan.requestFocus();
                        }
                    }, 200);
//            return true;
//        }
                    return false;
                }
            });

            completeButton.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick(View v) {

                    try {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                ECOMUserActivity.this);
                        alert.setTitle("Alert!!");
                        alert.setMessage("Do you want to complete or Continue");
                        alert.setPositiveButton("Complete", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                //do your work here
                                if (barcodeScan.size() > 0) {
                    /*docId = getApplicationContext().
                            getSharedPreferences(ParseOutscanXmlResponse.PREF_NAME, Context.MODE_PRIVATE).getString(pickUpId, null);
*/                  //dbHelper.trackScannedData();
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Sending updated scan item" + list + "PicUpId:" + pickUpId + ":" + assignId);

                                    try {
                                        if (savedList.size() > 0) {
                                            setScanData.addAll(list);
                                            List<String> newList = new ArrayList<String>(barcodeScan);
                                            sessionManager.setScannedDataSize(newList.size(), pickUpId);

                                            Crashlytics.log(android.util.Log.ERROR, TAG, "New List if:" + newList);
                                            dbHelper.updateECOMDetails(docId, "Y", newList, mLastUpdateTime, "NA", "NA", companyId, sessionManager.getLocId(), sessionManager.getuserId(), pickUpId, sessionManager.getDocType(pickUpId),"NA");
                                            //dbHelper.updateCompleteDoc(newList, companyId, locationId, pickUpId, assignId);
                                            //dbHelper.retrieveQueryData();
                                        } else {
                                            List<String> newList = new ArrayList<String>(barcodeScan);
                                            sessionManager.setScannedDataSize(newList.size(), pickUpId);
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "New List else:" + newList);
                                            dbHelper.updateECOMDetails(docId, "Y", newList, mLastUpdateTime, "NA", "NA", companyId, sessionManager.getLocId(), sessionManager.getuserId(), pickUpId, sessionManager.getDocType(pickUpId), "NA");
                                            //dbHelper.updateCompleteDoc(newList, companyId, locationId, pickUpId, assignId);
                                            //new LongOperation().execute();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

//                    Intent i = new Intent(getApplicationContext(), AssignedListMainActivity.class);
//                    startActivity(i);
                                    if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_ASSIGN")) {
                                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                                        editor = pref.edit();
//                        editor.clear();
                                        editor.putString("position", String.valueOf(1));
                                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                        editor.commit();
                                        Intent homeActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                                        startActivity(homeActivity);
                                    } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_COMPLETE")) {
                                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                                        editor = pref.edit();
//                        editor.clear();
                                        editor.putString("position", String.valueOf(1));
                                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                        editor.commit();
                                        Intent homeActivity = new Intent(getApplicationContext(), AssignmentCompleteStatus.class);
                                        startActivity(homeActivity);
                                    } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_INCOMPLETE")) {
                                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                                        editor = pref.edit();
//                        editor.clear();
                                        editor.putString("position", String.valueOf(1));
                                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                        editor.commit();
                                        Intent homeActivity = new Intent(getApplicationContext(), AssignmentInCompleteStatus.class);
                                        startActivity(homeActivity);
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Please scan the AWB's", Toast.LENGTH_LONG).show();
                                }
                                dialog.dismiss();
                                finish();
                                return;

                            }
                        });
                        alert.setNegativeButton("Continue", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                scan.setFocusable(true);
                                scan.requestFocus();
                                dialog.dismiss();
                            }
                        });

                        alert.create().show();
                    } catch (
                            Exception e
                            )

                    {
                        Crashlytics.log(android.util.Log.ERROR, TAG, e.getMessage());
                    }

                }
            });

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view,
                                               final int arg2, long arg3) {
                    try {

                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                ECOMUserActivity.this);
                        alert.setTitle("Alert!!");
                        alert.setMessage("Are you sure to delete record");
                        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do your work here
                                String scn_val = listView.getItemAtPosition(arg2).toString();
                                list.remove(arg2);
                                barcodeScan.remove(scn_val);
                                sessionManager.setScannedData(barcodeScan, pickUpId);
                                scn_cnt = barcodeScan.size();
                                tv_scn_count.setText(Integer.toString(scn_cnt));
                                adapter.notifyDataSetChanged();
                                dialog.dismiss();

                            }
                        });
                        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        alert.create().show();
                    } catch (Exception e) {

                        Crashlytics.log(android.util.Log.ERROR, TAG,e.getMessage());
                    }

                    return false;
                }
            });
        } catch (
                Exception e
                )

        {

        }

    }

    public void toggleMenu(View v) {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                ECOMUserActivity.this);
        alert.setTitle("Alert!!");
        alert.setMessage("Do you want to complete or Go back");
        alert.setPositiveButton("Complete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                //do your work here
                if (barcodeScan.size() > 0) {
                    /*docId = getApplicationContext().
                            getSharedPreferences(ParseOutscanXmlResponse.PREF_NAME, Context.MODE_PRIVATE).getString(pickUpId, null);
*/                  //dbHelper.trackScannedData();
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Sending updated scan item" + list + "PicUpId:" + pickUpId + ":" + assignId);

                    try {
                        if (savedList.size() > 0) {
                            setScanData.addAll(list);
                            List<String> newList = new ArrayList<String>(barcodeScan);
                            sessionManager.setScannedDataSize(newList.size(), pickUpId);

                            Crashlytics.log(android.util.Log.ERROR, TAG, "New List if:" + newList);
                            dbHelper.updateECOMDetails(docId, "Y", newList, mLastUpdateTime, "NA", "NA", companyId, sessionManager.getLocId(), sessionManager.getuserId(), pickUpId, sessionManager.getDocType(pickUpId),"NA");
                            //dbHelper.updateCompleteDoc(newList, companyId, locationId, pickUpId, assignId);
                            //dbHelper.retrieveQueryData();
                        } else {
                            List<String> newList = new ArrayList<String>(barcodeScan);
                            sessionManager.setScannedDataSize(newList.size(), pickUpId);
                            Crashlytics.log(android.util.Log.ERROR, TAG, "New List else:" + newList);
                            dbHelper.updateECOMDetails(docId, "Y", newList, mLastUpdateTime, "NA", "NA", companyId, sessionManager.getLocId(), sessionManager.getuserId(), pickUpId, sessionManager.getDocType(pickUpId),"NA");
                            //dbHelper.updateCompleteDoc(newList, companyId, locationId, pickUpId, assignId);
                            //new LongOperation().execute();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    Intent i = new Intent(getApplicationContext(), AssignedListMainActivity.class);
//                    startActivity(i);
                  /*  if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_ASSIGN")) {
                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                        editor = pref.edit();
//                        editor.clear();
                        editor.putString("position", String.valueOf(1));
                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                        editor.commit();
                        Intent homeActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                        startActivity(homeActivity);
                    } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_COMPLETE")) {
                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                        editor = pref.edit();
//                        editor.clear();
                        editor.putString("position", String.valueOf(1));
                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                        editor.commit();
                        Intent homeActivity = new Intent(getApplicationContext(), AssignmentCompleteStatus.class);
                        startActivity(homeActivity);
                    } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_INCOMPLETE")) {
                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                        editor = pref.edit();
//                        editor.clear();
                        editor.putString("position", String.valueOf(1));
                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                        editor.commit();
                        Intent homeActivity = new Intent(getApplicationContext(), AssignmentCompleteStatus.class);
                        startActivity(homeActivity);
                    }*/
                } else {
                    Toast.makeText(getApplicationContext(), "Please scan the AWB's", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
                finish();
                return;

            }
        });
        alert.setNegativeButton("Back", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
              /*  try {
                    sessionManager.setPosition(1);
                    if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_ASSIGN")) {
                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                        editor = pref.edit();
                        editor.clear();
                        editor.putString("position", String.valueOf(1));
                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                        editor.commit();
                        Intent homeActivity = new Intent(ECOMUserActivity.this, AssignedListMainActivity.class);
                        startActivity(homeActivity);
                    } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_COMPLETE")) {
                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                        editor = pref.edit();
                        editor.clear();
                        editor.putString("position", String.valueOf(1));
                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                        editor.commit();
                        Intent homeActivity = new Intent(ECOMUserActivity.this, AssignmentCompleteStatus.class);
                        startActivity(homeActivity);
                    } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_INCOMPLETE")) {
                        //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                        pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                        editor = pref.edit();
                        editor.clear();
                        editor.putString("position", String.valueOf(1));
                        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                        editor.commit();
                        Intent homeActivity = new Intent(ECOMUserActivity.this, AssignmentInCompleteStatus.class);
                        startActivity(homeActivity);
                    }
                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "toggleMenu" + e.getMessage());
                }*/

                dialog.dismiss();
                finish();
                return;
            }
        });

        alert.create().show();

    }

    @Override
    public void onBackPressed() {
        // do something on back.
    }

    private void playBeep() {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        //Define sound URI
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.fail)
                .setContentTitle("Alert")
                .setContentText("Barcode is not in the list")
                .setSound(soundUri); //This sets the sound to play

//Display notification
        notificationManager.notify(0, mBuilder.build());
    }

    private void popWindow() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.pop_window, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);

        Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
            }
        });
        popupWindow.showAsDropDown(completeButton, 8, 20);

    }

}

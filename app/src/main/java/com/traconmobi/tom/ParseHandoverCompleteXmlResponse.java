//package com.navatech.internlmodule;
//
//public class ParseHoldReason_XmlResponse {
//
//}
package com.traconmobi.tom;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;

//import net.sqlcipher.database.SQLiteConstraintException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import android.util.Xml;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteConstraintException;

public class ParseHandoverCompleteXmlResponse {

	private ArrayList<HashMap<String,String>> parsedDelCompleteList;
	protected SQLiteDatabase db;
	private StringReader xmlReader;
	
	public String hndovr_usr_id_Value=null;
	public String hndovr_sync_Value=null;
	public String hndovr_cust_acc_no_Value=null;
	public String hndovr_loc_code_Value=null;
	
//	public String DEL_isPickedValue=null;
	
	public static String hndovr_usr_idVal,hndovr_cust_acc_noVal,hndovr_syncVal,hndovr_loc_codeVal;
	
	private final String starttagList = "Table";	
	private final String hndovr_usr_id = "ASSIGN_NO";	
	private final String hndovr_sync = "SYNC";	
//	private final String hndovr_cust_acc_no="CUST_ACC_NO";
//	private final String hndovr_loc_code = "LOC_CD";	
//	private final String DEL_isDelivered = "isDelivered";	 
	
	 // Session Manager Class
    SessionManager session;
    public String session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;
	// variable to hold context
    public Context context;
    protected SQLiteDatabase db_insert;
		
	Cursor cursor,c;
	
	public ParseHandoverCompleteXmlResponse(String xml,Context context)
	{
		try
		{
		xmlReader = new StringReader(xml);
		this.context=context;
	    // Session class instance
        session = new SessionManager(context);

        /**GETTING SESSION VALUES**/
  
        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
         
        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
        
        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
        
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
         
        // session_USER_LOC
        session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);	      
         
        // session_USER_NUMERIC_ID
        session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
      
        // session_CURRENT_DT
        session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
       
        // session_CUST_ACC_CODE
        session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
        
        // session_USER_NAME
        session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
        
		}
		catch(Exception e)
		{
			e.getStackTrace();
			System.out.println("Exception ParseLoginAuthenticateXmlResponse" + e.getMessage());
		}
	
//		xmlReader = new StringReader(xml);
	
	}
	
	public void parse() throws XmlPullParserException, IOException 
	{
	
		
		TreeMap<String, String> DelCompleteListObj = null;	
		XmlPullParser parser = Xml.newPullParser();	
		parser.setInput(xmlReader);	
		int eventType = parser.getEventType();
	
		DelCompleteListObj = new TreeMap<String, String>();	
		
		parsedDelCompleteList = new ArrayList<HashMap<String, String>>();	
		while (eventType != XmlPullParser.END_DOCUMENT)
		{	
			String xmlNodeName = parser.getName();
			if (XmlPullParser.START_TAG == eventType) 
			{			
			xmlNodeName = parser.getName();
				if (xmlNodeName.equalsIgnoreCase(hndovr_usr_id)) 
				{
					hndovr_usr_id_Value = parser.nextText().toString();			
//				Log.d("AWB_ID",outscan_awbidValue);			
					DelCompleteListObj.put("hndovr_usr_id",hndovr_usr_id_Value);	
//					System.out.println("hndovr_usr_id_Value " + hndovr_usr_id_Value);
				}
//
			else if (xmlNodeName.equalsIgnoreCase(hndovr_sync)) 
				{
				hndovr_sync_Value = parser.nextText().toString();				
				DelCompleteListObj.put("hndovr_sync", hndovr_sync_Value);				
//				Log.d("AWB_NUM",outscan_awbnoValue);				
				}
//			else if (xmlNodeName.equalsIgnoreCase(hndovr_loc_code)) 
//			{
//				hndovr_loc_code_Value = parser.nextText().toString();				
//			DelCompleteListObj.put("hndovr_loc_code", hndovr_loc_code_Value);				
////			Log.d("AWB_NUM",outscan_awbnoValue);				
//			}
//				else if (xmlNodeName.equalsIgnoreCase(hndovr_cust_acc_no)) 
//				{
//					hndovr_cust_acc_no_Value = parser.nextText().toString();				
//				 DelCompleteListObj.put("hndovr_cust_acc_no",hndovr_cust_acc_no_Value);	
////				 System.out.println("ud_cust_acc_no_Value " + ud_cust_acc_no_Value);
////				System.out.println("DEL_returnd_assgnidValue " + DEL_returnd_assgnidValue);
////				Log.d("OUTSCAN_DTTME",outscan_dttmeValue);				
//				}
				
//			else if (xmlNodeName.equalsIgnoreCase(DEL_returnd_assgn_no)) 
//			{
//				DEL_returnd_assgn_no_Value = parser.nextText().toString();				
//				DelCompleteListObj.put("DEL_returnd_assgn_no", DEL_returnd_assgn_no_Value);				
////			Log.d("OUTSCAN_DTTME",outscan_dttmeValue);				
//			}
//			else if (xmlNodeName.equalsIgnoreCase(DEL_isDelivered)) 
//				{			
//				DEL_isPickedValue = parser.nextText().toString();			
//				DelCompleteListObj.put("DEL_isDeliveredValue", DEL_isPickedValue);			
////				Log.d("Consignee_Name",consignee_nmeValue);			
//				}			
			
		  } else if (XmlPullParser.END_TAG == eventType) 
		  {			
			if (xmlNodeName.equalsIgnoreCase(starttagList)) 
			{
//			  System.out.println("PODListObj" + PODListObj.size());
//			  System.out.println("DELListObj" + DELListObj.size());
			 
			
//			System.out.println("PODListObj value : "
//			
//			+ PODListObj.values());
//			System.out.println("DELListObj value : "
//					
//			+ DELListObj.values());
			
//			System.out.println("PODListObj show : " +PODListObj);
//			System.out.println("DELListObj show : " +DELListObj);
			
//			
//			parsedPODList.add(new HashMap<String, String>(
//			
//			PODListObj));
			
				parsedDelCompleteList.add(new HashMap<String, String>(
					
						DelCompleteListObj));
			
//			for (@SuppressWarnings("unused") Entry<String, String> entry : PODListObj.entrySet()) 
//			{		
//			    POD_awbid =PODListObj.get("POD_returnd_awbid");			
////			    Log.i("POD_awbid", POD_awbid);
//			    POD_awbno=PODListObj.get("POD_returnd_awbno");	
//
//			}
			for (Entry<String, String> entry : DelCompleteListObj.entrySet()) 
			{		
				hndovr_usr_idVal =DelCompleteListObj.get("hndovr_usr_id");
				hndovr_syncVal =DelCompleteListObj.get("hndovr_sync");
				
				System.out.println("show hndovr_sync_Value " + hndovr_usr_idVal + hndovr_sync_Value);
				if(hndovr_sync_Value.equals("Y"))
				{
					hndovr_sync_Value="1";
				}
				else if(hndovr_sync_Value.equals("N"))
				{
					hndovr_sync_Value="0";
				}
//				hndovr_loc_codeVal =DelCompleteListObj.get("hndovr_loc_code");
//				hndovr_cust_acc_noVal =DelCompleteListObj.get("hndovr_cust_acc_no");
				
//				System.out.println("dest_loc_codeVal " + hold_reason_codeVal + hold_reason_descVal);
//				 ContentValues cv =new ContentValues();			
////				    cv.put("T_U_ID",u_id);			
//				    cv.put("T_hndovr_usr_id",hndovr_usr_id_Value);	
//				    cv.put("T_hndovr_sync",hndovr_sync_Value);	
//				    cv.put("T_Loc_Cd",hndovr_loc_code_Value);	
//				    cv.put("T_Cust_Acc_NO",hndovr_cust_acc_no_Value);
//				    cv.put("D_DCREATE_DT",session_CURRENT_DT);
//				    System.out.println("parser reason_code "+ hndovr_usr_id_Value + ud_cust_acc_no_Value + session_CURRENT_DT  );
//				    cv.put("T_CITY_CODE",u_city_code);
//				    cv.put("T_LOC_TYPE",u_loc_type);
//				    cv.put("T_IMEI",u_IMEI_num);
//				    cv.put("T_Cust_Acc_No",Cust_acc_num);
//				    cv.put("CNAME",Cust_name);
//				    cv.put("CEMAIL",Cust_email);
//				    cv.put("D_ServerDate",serverdateValue);
				    
				    try{	

				    	if(session_DB_PATH != null && session_DB_PWD != null)
				      	  {
//					    	db_insert=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
				    		db_insert=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//					    	db_insert.beginTransaction();
				    		System.out.println("b4 updte " + hndovr_sync_Value + hndovr_usr_idVal);
				    		c=db_insert.rawQuery("UPDATE TOM_HandOver_Assignments SET C_is_Sync='" + hndovr_sync_Value+ "' where T_Assignment_Id='" + hndovr_usr_idVal + "'", null);
				    		while(c.moveToNext())
				    		{
				    			
				    		}
//				    		c.close();
//					    	db_insert.insertWithOnConflict("Transfer_User_MASTER", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_REPLACE);
//					    	System.out.println("TOM_Assignments transfer user " + cv);
//					    	db_insert.setTransactionSuccessful();
//					    	db_insert.endTransaction();	
//				            db_insert.close();
		              	  }
					    	else
							{
								System.out.println("Error Login parser session_DB_PATH is null ");
							}
				        }	
			           catch(SQLiteConstraintException ex){
			
			           //what ever you want to do		
			        	   System.out.println("Exception session_DB_PATH " + ex.getMessage());
			           }
				    finally
				    {
				    	if(c !=null && !c.isClosed())
				    	{
				    		c.close();
				    	}
				    	
				    	if(db_insert !=null && db_insert.isOpen())
				    	{
				    		 db_insert.close();
				    	}
				    	
				    }

				    /***For Loop close****/

			}
//			System.out.println("PODListObj value 2 : "
//			
//			+ PODListObj);
			
//			PODListObj.clear();
			
//			System.out.println(parsedPODList);	
			
//			System.out.println("DELListObj value 2 : "
//					
//			+ DELListObj);
			
			DelCompleteListObj.clear();
			
//			System.out.println(parsedDELList);
	}
			
	}			
			eventType = parser.next();	
		}
}

@SuppressWarnings("unused")
private String readText(XmlPullParser parser) throws IOException,

    XmlPullParserException {

String result = "";

if (parser.next() == XmlPullParser.TEXT) {

    result = parser.getText();

//    Log.d("wat is the result",result);

    parser.nextTag();

}

return result;

}

public ArrayList getparsedDelCompleteList() {

return parsedDelCompleteList;

}
}


//package com.navatech.tom;
//
//public class ParseHandoverCompleteXmlResponse {
//
//}



//package com.navatech.tom;
//
//public class ParseHandoverCompleteXmlResponse {
//
//}

package com.traconmobi.tom.rest.service;

import com.traconmobi.tom.rest.model.LocationParser;
import com.traconmobi.tom.rest.model.TokenParser;

import java.util.Map;

import retrofit.Call;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kumargaurav on 6/14/16.
 */

public interface LocationParserService {

    @FormUrlEncoded
    @POST("/UserTrack-History")
    Call<LocationParser> getLocationParserService(@FieldMap Map<String, Object> names);
}


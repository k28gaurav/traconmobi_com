package com.traconmobi.tom.rest.service;

import com.traconmobi.tom.rest.model.ApiResponse;
import com.traconmobi.tom.rest.model.ApiResponse1;

import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.http.Url;

/**
 * Created by kumargaurav on 12/22/15.
 */
public interface PickUpList {
    @GET
    Call<ApiResponse1> getRVPList(@Url String url,@QueryMap Map<String, String> data);
}

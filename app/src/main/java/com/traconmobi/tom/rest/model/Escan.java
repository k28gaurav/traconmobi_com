package com.traconmobi.tom.rest.model;

import com.google.gson.annotations.SerializedName;
import com.traconmobi.tom.model.Key2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 2/6/16.
 */
public class Escan {

    @SerializedName("data")
    private List<Key2> data = new ArrayList<Key2>();

    /**
     *
     * @return
     * The data
     */
    public List<Key2> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<Key2> data) {
        this.data = data;
    }
}

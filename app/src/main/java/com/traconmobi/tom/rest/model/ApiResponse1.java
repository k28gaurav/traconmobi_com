package com.traconmobi.tom.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.traconmobi.tom.model.Row1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 12/22/15.
 */

public class ApiResponse1 {

    @SerializedName("total_rows")
    private Integer totalRows;
    @SerializedName("rows")
    private List<Row1> rows = new ArrayList<Row1>();

    /**
     *
     * @return
     * The totalRows
     */
    public Integer getTotalRows() {
        return totalRows;
    }

    /**
     *
     * @param totalRows
     * The total_rows
     */
    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }

    /**
     *
     * @return
     * The rows
     */
    public List<Row1> getRows() {
        return rows;
    }

    /**
     *
     * @param rows
     * The rows
     */
    public void setRows(List<Row1> rows) {
        this.rows = rows;
    }

}


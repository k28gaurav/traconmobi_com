package com.traconmobi.tom;

import android.view.View;

/**
 * Created by kumargaurav on 11/17/15.
 */
public interface OnItemTouchListener {

    public void onCardViewTap(View view, int position);
    public void onButtonCancelClick(View view, int position);
    public void onButtonMapClick(View view, int position);
    public void onButtonCallClick(View view, int position);
    public void onRVPTextClick(View view, int position);
}

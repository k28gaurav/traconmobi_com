/*************************************************************/
/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 23/02/2014 **************************/
/***********PURPOSE   : HTTPLISTENER IS SUPER CLASS WHICH
 *                      SET THE HTTP OR HTTPS REQUEST ******/
/************************************************************/
package com.traconmobi.tom.http;

/**
 *
 *HttpListener is super class of which set the HTTP or  HTTPS request 
 */
public interface HttpListner {
    /**
     * @param http
     * Http handler object. when http request is completed then is called in HttpHandler class
     */
    void notifyHTTPRespons(HttpHandler http);
}

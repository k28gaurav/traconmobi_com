package com.traconmobi.tom.http;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.SessionManager;
import com.traconmobi.tom.rest.model.TokenParser;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by kumargaurav on 3/28/16.
 */
public class ParseAuthenticate {
    // Session Manager Class

    SessionManager session;
    private String session_DB_PATH,session_DB_PWD;
    // variable to hold context
    private Context context;
    protected SQLiteDatabase db_insert;
    private String token = "";
    private String TAG = "ParseAuthenticate";

    public ParseAuthenticate(Context ctx) {
        Fabric.with(context, new Crashlytics());

        // Session class instance
        session = new SessionManager(ctx);

        /**GETTING SESSION VALUES**/

        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
    }

    public String storeInDb(TokenParser apiResponse) {
        if(apiResponse.getToken() != null) {
            Log.e(TAG,"StoreInDb called");
            token = apiResponse.getToken();
            String u_lastlogin_dt= apiResponse.getServerDateTime();
            String usr_last_dt=u_lastlogin_dt.substring(0, 10);
            String user_tme=u_lastlogin_dt.substring(11, 19);
            ContentValues cv = new ContentValues();
            cv.put("T_U_ID", apiResponse.getUserId());
            cv.put("T_username", apiResponse.getUserName());
            cv.put("T_password", apiResponse.getPassword());
            cv.put("T_Loc_Cd", apiResponse.getLocCode());
            cv.put("D_User_last_login",  usr_last_dt);
            cv.put("D_User_last_login_time", user_tme);
            cv.put("T_IMEI", apiResponse.getIMEI());
            cv.put("T_Cust_Acc_No", apiResponse.getCompanyID());
            cv.put("T_CompanyName", apiResponse.getCompanyName());
            cv.put("T_Loc_Id", apiResponse.getLocID());
            cv.put("D_ServerDate", usr_last_dt);
            try {

                if (session_DB_PATH != null && session_DB_PWD != null) {
                    Log.e(TAG,"StoreInDb called when session_DB_PATH not null");
//				    	db_insert=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                    db_insert = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                    db_insert.beginTransaction();
                    db_insert.insertWithOnConflict("TOM_Users", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_REPLACE);
                    db_insert.setTransactionSuccessful();
                    db_insert.endTransaction();
                    db_insert.close();
                } else {
                    Crashlytics.log(Log.ERROR, TAG, "Error Login parser session_DB_PATH is null ");
                }
            } catch (SQLiteConstraintException ex) {
                //what ever you want to do
                Crashlytics.log(Log.ERROR, TAG, "Exception session_DB_PATH " + ex.getMessage());
            }
        }
        return token;
    }
}

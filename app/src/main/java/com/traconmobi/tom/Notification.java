package com.traconmobi.tom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by kumargaurav on 5/5/16.
 */
public class Notification extends Activity {
    SessionManager session;
    TextView version_name,title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_notify);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_notification);
            session = new SessionManager(getApplicationContext());
            title=(TextView)findViewById(R.id.txt_title);
            title.setText("NOTIFICATION");
            TextView text = (TextView)findViewById(R.id.notification_text);
            text.setText(session.getAssignedShippmentSize() + " New Pickup Shippment Arrived");
            session.setNotify(false);
            session.setAssignedShippmentSize(0);
        } catch (Exception e) {

        }
    }

    public void toggleMenu(View v)
    {
        Intent homeActivity = new Intent(this, HomeMainActivity.class);
        startActivity(homeActivity);
    }
}

package com.traconmobi.tom;



import android.app.ProgressDialog;
import android.content.Context;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.adapter.DeliveryAdapter;
import com.traconmobi.tom.adapter.ItemRow;


import android.view.*;

import android.widget.AdapterView.OnItemClickListener;

import io.fabric.sdk.android.Fabric;


public class DeliverySingleFragment extends Fragment implements BaseFragment {

    // Session Manager Class
    String searchQuery;
    String address1,address2,city_detail;
    List<ItemRow> items;
    DeliveryAdapter deliveryAdapter;
    SessionManager session;
    public String session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME, session_DB_PATH, session_DB_PWD;
    protected SQLiteDatabase db;
    protected Cursor cur, cursor, cursor_outscan_dt, cr, cursor_srch;
    public static String Assignment_Type, fltr_type, t_u_id, cod_amt_val;
    String strFilter = "", index, awb_outscantime, awb_outscan_dt;

    protected ListView employeeList;
    protected EditText searchText;

    static final String EXTRA_MESSAGE = null;
    private final static String TAG = "DeliverySingleFragment";
    boolean mShowingChild;
    Button Onclick_search, Onclick_multiple;
    public PageFragmentListener mListener;

    public DeliverySingleFragment() {
    }

    public DeliverySingleFragment(PageFragmentListener listener) {
        mListener = listener;
    }

    public static DeliverySingleFragment newInstance(PageFragmentListener listener) {
        DeliverySingleFragment fragment = new DeliverySingleFragment();
        return new DeliverySingleFragment(listener);
    }

    public static DeliverySingleFragment newInstance() {
        return new DeliverySingleFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_delivery, container, false);

        try {
            //Intializing Fabric
            Fabric.with(getActivity(), new Crashlytics());

            // Session class instance
            session = new SessionManager(getActivity().getApplicationContext());

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);

            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();
            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);
            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);
            // get user data from session
            HashMap<String, String> user = session.getUserDetails();
            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);
            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);
            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);
            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);
            searchText = (EditText) rootView.findViewById(R.id.searchText);
            employeeList = (ListView) rootView.findViewById(R.id.list);
            Onclick_multiple = (Button) rootView.findViewById(R.id.multiplButton);

            Onclick_multiple.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onSwitchToNextFragment();
                    }
                }
            });

            searchText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        // || is the concatenation operation in SQLite
//            EditText srch =(EditText)view.findViewById(R.id.searchText);
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print text " + searchText.getText().toString().trim());
                        String srch_text = searchText.getText().toString().trim();
                        if (srch_text.isEmpty()) {
//                            Toast.makeText(getActivity(), "Please enter the Order number", Toast.LENGTH_LONG).show();

                        } else if (!(srch_text.isEmpty())) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "print text not empty" + searchText.getText().toString().trim());
//	    	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                            Assignment_Type = "D";
                            /**cursor = db.rawQuery("SELECT _id , AWB_NUM, Consignee_Name, Address_Line1, Address_Line2 ,City ,CellPhone,Pincode,COD_Amount FROM TOM_Del WHERE AWB_NUM LIKE  '%"+ searchText.getText().toString().trim() +"%' and isDelivered IS NULL or(AWB_NUM LIKE   '%"+ searchText.getText().toString().trim() +"%' and isDelivered=0 )", null);**/
                            cursor_srch = db.rawQuery("SELECT _id ,D_OutScan_Date, T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Amount_Type,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Number LIKE  '%" + searchText.getText().toString().trim() + "%' AND T_Assignment_Type='" + Assignment_Type + "'  AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' AND D_OutScan_Date='" + session_CURRENT_DT + "'", null);
                            // new String[]{"" + searchText.getText().toString() + ""});
                            int cur_search = cursor_srch.getCount();
                            if (cur_search > 0) {
                                final MyCursorAdapter adapter = new MyCursorAdapter(
                                        getActivity(),
                                        R.layout.delivery_list_items,
                                        cursor_srch,
                                        new String[]{"T_Assignment_Number", "T_Contact_number", "T_Consignee_Name", "T_Address_Line1", "F_Amount","T_Amount_Type"},
                                        //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
                                        new int[]{R.id.txtordrval, R.drawable.call, R.id.txtName, R.id.txtADDR, R.id.txtrsval,R.id.cod}, 0);


                                employeeList.setAdapter(adapter);
                                employeeList.setCacheColorHint(Color.WHITE);
                                employeeList.setVisibility(View.VISIBLE);
                                employeeList.setOnItemClickListener(new OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        // TODO Auto-generated method stub
                                        Intent intent = new Intent(getActivity().getApplicationContext(), AssgnDtlsMainActivity.class);
                                        Cursor cursor = (Cursor) adapter.getItem(position);
                                        intent.putExtra("EMPLOYEE_ID", cursor.getInt(cursor.getColumnIndex("_id")));
                                        index = cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
                                        intent.putExtra("EXTRA_MESSAGE", index);
                                        startActivity(intent);
                                    }
                                });
                            } else if (cur_search == 0) {
                                employeeList.setVisibility(View.INVISIBLE);
                                Toast toast = Toast.makeText(getActivity(), "Given Order Number Doesnot Exist", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                searchText.setText("");

                            }
                        }
                    } catch (SQLiteException e) {
//  			 onClickGoToHomePage();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity search SQLiteException ");
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity search Exception ");
//				onClickGoToHomePage();
                    } catch (UnsatisfiedLinkError err) {
                        err.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity search UnsatisfiedLinkError ");
//    		onClickGoToHomePage();
                    } finally {

                        if (db != null) {
                            db.close();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            items = new ArrayList<ItemRow>();
            deliveryAdapter = new DeliveryAdapter(getActivity(),getActivity().getApplicationContext(), items);
            employeeList.setAdapter(deliveryAdapter);
            employeeList.setCacheColorHint(Color.WHITE);
            searchQuery = searchText.getText().toString().trim();
            Log.e(TAG, searchQuery);
            new LoadTask(searchQuery).execute();
            //shwsinglelist();
        } catch (Exception e)

        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception DeliverySingleFragment Oncreate" + e.getMessage());
        }
        return rootView;
    }

    //@Override
    public void onBackPressed() {

    }



    @Override
    public boolean isShowingChild() {
        return mShowingChild;
    }

    @Override
    public void setShowingChild(boolean showingChild) {
        mShowingChild = showingChild;
    }

    private class LoadTask extends AsyncTask<Void, Void, Void> {
        String searchParam;
        final ProgressDialog dialog = new ProgressDialog((AssignedListMainActivity)getActivity());


        LoadTask(String searchText) {
            searchParam = searchText;
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for Deliver list items to prepare...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }

        @Override
        protected Void doInBackground(Void... param) {
            try {
                if (session_DB_PATH != null && session_DB_PWD != null) {
                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS |
                            SQLiteDatabase.OPEN_READWRITE);
                    Assignment_Type = "D";
                    fltr_type = "A";
                    cursor_outscan_dt = db.rawQuery("SELECT _id,D_OutScan_Date, T_Assignment_Number,T_Consignee_Name," +
                            " T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name" +
                            " FROM TOM_Assignments WHERE T_Consignee_Name LIKE  '%" + searchParam + "%' AND T_Assignment_Type='"
                            + Assignment_Type + "'  AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +
                            "' AND D_OutScan_Date='" + session_CURRENT_DT + "'", null);

                    while (cursor_outscan_dt.moveToNext()) {
                        awb_outscantime = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
                        awb_outscan_dt = awb_outscantime.substring(0, 10);
                        cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
                        session.createSingleCODSession(cod_amt_val);
                    }

                    cursor_outscan_dt.close();
                    db.close();
                    if (awb_outscan_dt != null) //if(awb_outscan_dt.equals(session_CURRENT_DT))
                    {
                        db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Assignment_Type = "D";
                        fltr_type = "A";
                        cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ," +
                                "T_City ,T_Contact_number,T_Pincode,F_Amount,T_Amount_Type,T_Shipper_name FROM TOM_Assignments" +
                                " WHERE T_Consignee_Name LIKE  '%" + searchParam + "%' AND T_Assignment_Type='" + Assignment_Type + "'  " +
                                "AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' AND D_OutScan_Date='"
                                + session_CURRENT_DT + "'" +
                                "", null);

                        while (cur.moveToNext()) {
                            items.add(new ItemRow(cur.getString(cur.getColumnIndex("_id")), cur.getString(cur.getColumnIndex("T_Assignment_Number")),
                                    cur.getString(cur.getColumnIndex("T_Contact_number")), cur.getString(cur.getColumnIndex("T_Consignee_Name")),
                                    cur.getString(cur.getColumnIndex("T_Address_Line1")), cur.getString(cur.getColumnIndex("T_Address_Line2")),
                                    cur.getString(cur.getColumnIndex("F_Amount")), cur.getString(cur.getColumnIndex("T_City")),
                                    cur.getString(cur.getColumnIndex("T_Amount_Type"))));
                        }
                        cur.close();
                        if(db != null) {
                            db.close();
                        }
                    }
                }
            }catch(SQLException e) {
                if(db != null) {
                    db.close();
                }
            }
            catch (Exception e) {
                Log.e("DeliverySingleFragment", "" + e.getMessage());
                if(db != null) {
                    db.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            deliveryAdapter.notifyDataSetChanged();
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        }
    }



/**************************EXTEND THE SIMPLECURSORADAPTER TO CREATE A CUSTOM CLASS WHERE WE CAN OVERRIDE THE GETVIEW TO CHANGE THE ROW COLORS**************************/
private class MyCursorAdapter extends SimpleCursorAdapter{
    @SuppressWarnings("deprecation")
    public MyCursorAdapter(Context context, int layout, Cursor c,
                           String[] from, int[] to, int flags) {
        super(context, layout, c, from, to);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        //***********************get reference to the row **********************/
        View view = super.getView(position, convertView, parent);
        if (view == null){
            LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.delivery_list_items, null);
        }
        try
        {

            ImageView textdir = (ImageView) view.findViewById(R.id.btnmap);

/************************ CALLED WHEN USER CLICKS GET DIRECTION TEXTLINK ************************/

                textdir.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        @SuppressWarnings("unused")
                        int selectedid = position;
                        RelativeLayout rl = (RelativeLayout) v.getParent();
                        TextView textawb = (TextView) rl.findViewById(R.id.txtordrval);
                        String getawb = textawb.getText().toString();

//                    db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Cursor csr = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Consignee_Name,emp.T_Address_Line1, emp.T_Address_Line2,emp.T_City,emp.T_Contact_number,emp.T_Pincode FROM TOM_Assignments emp where emp.T_Assignment_Number = ?",
                                new String[]{"" + getawb});

                        while (csr.moveToNext()) {
                            address1 = csr.getString(csr.getColumnIndex("T_Address_Line1"));
                            address2 = csr.getString(csr.getColumnIndex("T_Address_Line2"));
                            city_detail = csr.getString(csr.getColumnIndex("T_City"));
                        }
                        csr.close();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/maps?daddr=" + address1 + address2 + city_detail + "&dirflg=r"));
//                        if (isAppInstalled("com.google.android.apps.maps")){
//                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                        }
                        db.close();
                        startActivity(intent);
//                    finish()
                    }
                });

            /********************** CALLED WHEN USER CLICKS PHONENUMBER TEXTLINK *********************/


                ImageView txtcellphn = (ImageView) view.findViewById(R.id.btncall);
                txtcellphn.setOnClickListener(new OnClickListener() {
                    @Override
                    @SuppressWarnings("static-access")
                    public void onClick(View v) {
                        String celphno = "";
                        RelativeLayout rl = (RelativeLayout) v.getParent();
                        TextView textawb = (TextView) rl.findViewById(R.id.txtordrval);
                        String getawb = textawb.getText().toString();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "getawb" + getawb);

//                    db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Cursor cr = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Consignee_Name,emp.T_Address_Line1,emp.T_Address_Line2,emp.T_City,emp.T_Contact_number,emp.T_Pincode FROM TOM_Assignments emp where emp.T_Assignment_Number = ?",
                                new String[]{"" + getawb});
                        while (cr.moveToNext()) {
                            celphno = cr.getString(cr.getColumnIndex("T_Contact_number"));
                        }
                        cr.close();
                        PhoneCall phcl = new PhoneCall();
                        phcl.call(celphno, getActivity());
//                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//                    callIntent.setData(Uri.parse("tel:" + celphno));
//                    startActivity(callIntent);
                        db.close();


                    }
                });
            if(position % 2 == 0){
                view.setBackgroundColor(Color.rgb(255, 255, 255));
                //  view.setBackgroundColor(color.opaque_back);
            }else {
                view.setBackgroundColor(Color.rgb(255, 255, 255));
            }
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception getView " + e.getMessage());
        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity UnsatisfiedLinkError ");
//        		onClickGoToHomePage();
        }
        return view;
    }
}
    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();
        //items.clear();

    }

}

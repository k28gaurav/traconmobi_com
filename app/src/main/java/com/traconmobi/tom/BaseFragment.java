package com.traconmobi.tom;

import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;

/**
 * Created by LENOVO on 12/5/2015.
 */

public interface BaseFragment{

   public boolean mShowingChild=false;

   public PageFragmentListener mListener = null;

    boolean isShowingChild();

    void setShowingChild(boolean showingChild) ;
}

package com.traconmobi.tom.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.rest.RestClient;

/**
 * Created by kumargaurav on 12/8/15.
 */
public class App extends MultiDexApplication
{
    private static RestClient restClient;
    static String TAG = "App";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static RestClient getRestClient()
    {
        try
        {
        restClient = new RestClient();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR,TAG,e.getMessage());
        }
        return restClient;
    }
}

